from FeatureFunctions import *

n_windows_per_sensor = 4 # Number of window configurations per sensor
min_features_default = 5 # Minimum number of features to select
max_features_default = 20 # Maximum number of features to select

# Chosen sensor data to use
sensors_default = ["EDA", "BVP", "HR", "TEMP"]

# Chosen good feature functions
feature_funcs_default = [
    np.mean,
    np.std,
    np.max,
    np.min,
    np.ptp,
    np_1st_deriv,
    np_2nd_deriv,
    fl_delta,
#     np_1st_diff,
#     np_2nd_diff,
#     entropy_of_abs,
]

# Default window configurations
w_configs_default = OrderedDict([
    ('EDA',  [
        [3.27, 0.95, 4.7],
        [0.6395, 0.731, 4.781],
        [6.8, 0.937, 1],
        [3.28, 0.937, 0.563],
    ]),
    ('BVP',  [
        [8, 0.9, 0],
        [2, 0.95, 0],
        [6.86, 0.9107, 0],
        [5, 0.95, 0],
    ]),
    ('HR',   [
        [6, 0.9, 5],
        [8, 0.9, 7],
        [2, 0.95, 1],
        [4, 0.95, 3],
    ]),
    ('TEMP', [
        [7, 0.95, 2],
        [5, 0.95, 4.563],
        [9, 0.95, 4.563],
        [5, 0.95, 0],
    ]),
])
final_window_size_default = 15
final_window_overlap_default = 0.95

# Ranges for window configuration parameters
# shape = (n_sensors, #[window size, overlap, shift], #[min, max])
w_config_ranges = np.asarray([
    [[0.56, 20], [0.7, 1.0], [0, 7]],
    [[0.065, 20], [0.7, 1.0], [0, 7]],
    [[1.65, 20],  [0.7, 1.0], [0, 7]],
    [[0.56, 20], [0.7, 1.0], [0, 7]],
])
final_window_size_range = [10, 20]
final_window_overlap_range = [0.7, 1.0]

