'''

    This file contains shared constants

'''

import multiprocessing


# System specific options - remember to set these again in new environments!
n_cpu = multiprocessing.cpu_count()


data_dir = "data"
graphics_dir = "graphics"
learning_data_dir = "learning_data"

empatica_data_dir = "empatica_labelled"
selfreport_data_dir = "self_report"


# participants_all = ["RichardSterry", "LynrayBarends"]
participants_all = [
    "AngelosAngelides",
    "JacquelineDing",
    "JiaxuanLiu",
    "LynrayBarends",
    "NianchuanChen",
    "RichardSterry",
    "TobyLi",
    "YuanZhang",
    "ZeyuSong",
    "ZhilinPan",
]

sensors_all = ["ACC", "TEMP", "HR", "BVP", "EDA"]
sensors_all_n_vals = dict(zip(sensors_all, [3] + ([1] * len(sensors_all))))

emo_labels_all = [
    "Arousal",
    "Valence",
    "Positive Nostalgia",
    "Negative Nostalgia",
    "Affinity",
]
emo_miss_values = [0, 0, 0, 0, 5]

n_songs_per_participant = 8 # Max number of songs per participant
data_song_length = 120 # Length of song/baseline


# Good window configs:

## Positive Nostalgia
# EDA
# [0.6395, 0.731, 4.781]
# BVP
# [20, 0.7, 0]
# [8.0571 0.99786 3.3126]

## Affinity

