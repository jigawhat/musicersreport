#
# Runs everything
#

from FinalModel import *


if __name__ == "__main__":

    # Run the full final model pipeline training & testing, plotting and saving graphs/results to the "graphics" folder
    run_final_pipeline(
        # Optimise window configurations with bayesian hyperparameter optimisation
        optimise_windows=False,
        # Select features with sequential forward floating selection
        select_features=True,
        # Whether to perform the subject-included optimisation
        perform_subject_included=False,
    )


