#
# This script converts the Nostalgic music tsv file into a more readable HTML table (color keyed)
#

from Utils import *
pd.set_option('display.max_colwidth', 0)

# Script options
grouping_decades = [ 1970, 1980, 1990, 2000 ]
grouping_decades_labels = ["Pre-70's", "70's", "80's", "90's", "00's+"]

genres = pd.read_csv(data_dir + "nostalgic_music_genres.tsv", sep='\t', index_col=0)
songs = pd.read_csv(data_dir + "nostalgic_music.tsv", sep='\t')

def populate_cell(df):
    r = ""
    for i in df.index:
        r += "<div class=\"song " + songs.loc[i, 'emotion'].lower() + "\">" + songs.loc[i, 'name'] + "</div>"
    return r

df, dec_i = [], 0
cur_year = -sys.maxsize
for year in grouping_decades + [sys.maxsize]:
    df += [["<b>" + grouping_decades_labels[dec_i] + "</b>"]]
    for g in genres.index:
        df[-1] += [ populate_cell(songs[(songs["year"] > cur_year) & (songs["year"] < year) & (songs['genre'] == g)]) ]
    cur_year = year
    dec_i += 1
df = pd.DataFrame(df)
df.index = grouping_decades_labels
df.columns = ["Decade"] + ['<center>' + genres.loc[x][0].replace(', ', ',<br />') + '</center>' for x in genres.index]

with open(graphics_dir + "NostalgicMusic.html", 'w') as f:
    hdr="<html>" + '\n' + \
        "<head>" + '\n' + \
        "<style>" + '\n' + \
        "body {" + '\n' + \
        "    font-family: sans-serif;" + '\n' + \
        "    font-size: 10pt;" + '\n' + \
        "}" + '\n' + \
        ".dataframe td {" + '\n' + \
        "    white-space: nowrap;" + '\n' + \
        "}" + '\n' + \
        "div.output_subarea {" + '\n' + \
        "    overflow-x: inherit;" + '\n' + \
        "}" + '\n' + \
        ".musicTable {" + '\n' + \
        "    white-space: nowrap;" + '\n' + \
        "    padding: 0px 0px;" + '\n' + \
        "    border: 0px;" + '\n' + \
        "}" + '\n' + \
        ".song {" + '\n' + \
        "    font-size: 7pt;" + '\n' + \
        "}" + '\n' + \
        ".q1 {" + '\n' + \
        "    color: #0000FF;" + '\n' + \
        "}" + '\n' + \
        ".q2 {" + '\n' + \
        "    color: #009000;" + '\n' + \
        "}" + '\n' + \
        ".q3 {" + '\n' + \
        "    color: #EE9000;" + '\n' + \
        "}" + '\n' + \
        ".q4 {" + '\n' + \
        "    color: #FF0000;" + '\n' + \
        "}" + '\n' + \
        "</style>" + '\n' + \
        "</head>" + '\n' + \
        "<body>" + '\n' + \
        "<table>" + '\n' + \
        "  <thead>" + '\n' + \
        "    <tr>" + '\n' + \
        "      <th> Color Key (Approx Emotion Group) </th>" + '\n' + \
        "    </tr>" + '\n' + \
        "  </thead>" + '\n' + \
        "  <tbody>" + '\n' + \
        "    <tr>" + '\n' + \
        "      <th class='q1 song'> Q1 (Positive Valence, Positive Arousal) </th>" + '\n' + \
        "    </tr>" + '\n' + \
        "    <tr>" + '\n' + \
        "      <th class='q2 song'> Q2 (Positive Valence, Negative Arousal) </th>" + '\n' + \
        "    </tr>" + '\n' + \
        "    <tr>" + '\n' + \
        "      <th class='q3 song'> Q3 (Negative Valence, Negative Arousal) </th>" + '\n' + \
        "    </tr>" + '\n' + \
        "    <tr>" + '\n' + \
        "      <th class='q4 song'> Q4 (Negative Valence, Positive Arousal) </th>" + '\n' + \
        "    </tr>" + '\n' + \
        "  </tbody>" + '\n' + \
        "</table>" + '\n' + \
        "<br />" + '\n'
    ftr="\n</body>\n</html>"
    f.write(hdr)
    df.to_html(f, classes='musicTable', escape=False, na_rep="N/A", index=False)
    f.write(ftr)