#
#  Learning utilitions
#

from LearningData import *
from Models import *


# Define scoring functions
def multitask_scorer(sfunc, y_t, y_p):
    if len(y_t.shape) < 2 or len(y_p.shape) < 2:
        return sfunc(y_t, y_p)
    ys=[(y_t[:,i].flatten(),y_p[:, i].flatten()) for i in range(y_t.shape[1])]
    kwargs = {}
    if sfunc in have_averaging:
        kwargs["average"] = None
    if sfunc in for_regr:
        kwargs["multioutput"] = "raw_values"
    return np.array([sfunc(y, y_, **kwargs) for y, y_ in ys])

def multitask_acc(y_t, y_p):
    return multitask_scorer(accuracy_score, y_t, y_p)

def mean_relative_error(y_true, y_pred):
    return mean_absolute_error(y_true, y_pred) / y_true

scorers_c = [
    "multitask_acc",
    "f1_score",
    "recall_score",
    "precision_score",
]
scorers_r = [
    "mean_absolute_error",
    "mean_relative_error",
    "r2_score",
    "mean_squared_error"
]
sfuncs_lib = {
    "multitask_acc": multitask_acc,
    "f1_score": f1_score,
    "recall_score": recall_score,
    "precision_score": precision_score,
    "mean_absolute_error": mean_absolute_error,
    "mean_relative_error": mean_relative_error,
    "r2_score": r2_score,
    "mean_squared_error": mean_squared_error,
    "mean_squared_log_error": mean_squared_log_error,
    "explained_variance_score": explained_variance_score,
}

have_averaging = [f1_score, precision_score, recall_score]
for_regr=[mean_absolute_error,mean_relative_error,r2_score,mean_squared_error,
          mean_squared_log_error, explained_variance_score]


# Compute/score learning curves
def do_learning_curve(X, Y, X_test, models, ns_samples, ns_eval_samples):
    Y_1d = None
    if Y.ndim == 2 and Y.shape[1] == 1:
        Y_1d = Y.ravel()
    res = []
    print("Learning learning curve...")
    for model in models:
        print("model: " + model)
        res += [[]]
        nss = sum([[ns_samples[j]] * ns_eval_samples[j] \
            for j in range(len(ns_samples))], [])
        pres=Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(lc_test_model)(
            model, n_s, X, Y, Y_1d, X_test) for n_s in nss)
        pres_i = 0
        for j in range(len(ns_samples)):
            n_ev = ns_eval_samples[j]
            res[-1].append(pres[pres_i:pres_i + n_ev])
            pres_i += n_ev
    return res

def do_cv_learning_curve(X, Y, models, ns_samples, ns_eval_samples, cv=10):
    Y_1d = None
    if Y.ndim == 2 and Y.shape[1] == 1:
        Y_1d = Y.ravel()
    res = []
    print("Learning cross validated learning curve...")
    for model in models:
        print("model: " + model)
        t = time.time()
        res += [[]]
        nss = sum([[ns_samples[j]] * ns_eval_samples[j] \
            for j in range(len(ns_samples))], [])
        pres=Parallel(n_jobs=n_parallel_cpu, verbose=5)(delayed(lc_test_model)(
            model, n_s, X, Y, Y_1d, cv=cv) for n_s in nss)
        pres_i = 0
        for j in range(len(ns_samples)):
            n_ev = ns_eval_samples[j]
            res[-1].append(pres[pres_i:pres_i + n_ev])
            pres_i += n_ev
        t = time.time() - t
        sys_print('time taken (s): ' + str(t) + '\n')
    return res

def score_learning_curve(Y,Y_pr, models, ns_samples, ns_eval_samples, scorers):
    scores = []
    for i in range(len(models)):
        scores += [[]]
        for l in range(len(scorers)):
            scorer = sfuncs_lib[scorers[l]]
            scores[-1] += [[]]
            for j in range(len(ns_samples)):
                scores[-1][-1] += [[]]
                for k in range(ns_eval_samples[j]):
                    pr = Y_pr[i][j][k]
                    if pr.ndim > 1 and pr.shape[1] == 1:
                        pr = pr[:].flatten()
                    if pr.ndim == 1 and Y.ndim > 1:
                        Y = Y[:].flatten()
                    pr = (pr > 0.5).astype(int)
                    scores[-1][-1][-1] += [ scorer(Y, pr) if pr.ndim == 1 else
                            (scorer(Y, pr, multioutput="raw_values") if scorer \
                            in for_regr else (scorer(Y, pr, average=None) if \
                            scorer in have_averaging else scorer(Y, pr))) ]
    return scores

def score_learning_curve_cv(Y,Y_pr, models,ns_samples,ns_eval_samples,scorers):
    scores = []
    for i in range(len(models)):
        scores += [[]]
        for l in range(len(scorers)):
            scorer = sfuncs_lib[scorers[l]]
            scores[-1] += [[]]
            for j in range(len(ns_samples)):
                scores[-1][-1] += [[]]
                for k in range(ns_eval_samples[j]):
                    indices = Y_pr[i][j][k][1]
                    pr = Y_pr[i][j][k][0]
                    if pr.ndim > 1 and pr.shape[1] == 1:
                        pr = pr[:].flatten()
                    if pr.ndim == 1 and Y.ndim > 1:
                        Y = Y[:].flatten()
                    pr = (pr > 0.5).astype(int)
                    Y_ = Y[indices]
                    scores[-1][-1][-1] += [
                        scorer(Y_, pr, multioutput="raw_values") if scorer \
                        in for_regr else (scorer(Y_, pr, average=None) if \
                        scorer in have_averaging else scorer(Y_, pr)) ]
    return scores


# Test model for learning curve computation
def lc_test_model(mkey, n_s, X, Y, Y_1d, X_test=None, cv=None):
    inds = np.random.choice(X.shape[0], n_s, replace=False)
    m = model = get_model(mkey)
    if isinstance(model, Pipeline):
        m = m.steps[-1][1]
    Y_train = Y[inds] if Y_1d is None or (
        isinstance(m, MultiOutputClassifier) or \
        isinstance(m, MultiOutputRegressor)) else Y_1d[inds]
    if cv is not None:
        return cross_val_predict(model, X[inds], Y_train, cv=cv), inds
    model.fit(X[inds], Y_train)
    return model.predict(X_test)

# Test model for Feature importance notebook
def test_model(mkey, skey, X, Y, n_test):
    model = get_model(mkey)
    scorer = sfuncs_lib[skey]
    X, Y = shuffle_data([X, Y])
    X_train, Y_train, X_test, Y_test = \
        X[:-n_test], Y[:-n_test], X[-n_test:], Y[-n_test:]
    model.fit(X_train, Y_train)
    Y_pr = model.predict(X_test)
    return scorer(Y_test, Y_pr)[0]

# Test model for Win Probability Histograms notebook
def test_model_prob(mkey, X, Y, tier_inds, n_train, n_tier_test):
    m_ = m = model = get_model(mkey)
    if isinstance(model, Pipeline):
        m = m.steps[-1][1]
    if isinstance(m, MultiOutputClassifier):
        m_ = m.estimator

    tier_inds = tier_inds[:] # Get train & test data subsets
    np.random.shuffle(tier_inds)
    test_is = tier_inds[:n_tier_test]
    train_is = tier_inds[n_tier_test:]
    if len(train_is) < n_train:
        train_is = np.hstack([train_is, np.random.choice(list(set(range(
            len(X)))-set(tier_inds)), n_train-len(train_is), replace=False)])

    model.fit(X[train_is], Y[train_is] if \
        isinstance(m, MultiOutputClassifier) else Y[train_is].ravel())

    if hasattr(m_, "predict_proba"):
        res = model.predict_proba(X[test_is])
        if isinstance(res, list):
            res = res[0]
        res = res[:, 1]
    else:  # use decision function
        res = model.decision_function(X[test_is])
        res = (res - res.min()) / (res.max() - res.min())
    return res


