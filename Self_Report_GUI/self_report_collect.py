from Tkinter import *
import os
import csv

def save_self_report():
    
    save_path = (os.path.join('.',name.get()))
    print save_path

    if not os.path.exists(save_path):
        os.makedirs(save_path)
    
    report = []
    
    name_data       = ['Particiant_Name',name.get()]
    date_data       = ['Date',date.get()]
    song_data       = ['Song_ID',song_id.get()]
    arousal_data    = ['Arousal',arousal.get()]
    valence_data    = ['Valence',valence.get()]
    pnostalgia_data = ['Positive_Nostalgia',positive_nostalgia.get()]
    nnostalgia_data = ['Nagative_Nostalgia',nagative_nostalgia.get()]
    affinity_data   = ['Affinity',affinity.get()]
    
    report.append(name_data)
    report.append(date_data)
    report.append(song_data)
    report.append(arousal_data)
    report.append(valence_data)
    report.append(pnostalgia_data)
    report.append(nnostalgia_data)
    report.append(affinity_data)
   

    file_name = str(song_id.get()) + '.csv' 
    file_path = (os.path.join(save_path,file_name))

    print report
    with open(file_path, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(report)

    f.close()

    return 

def clean_entry():
    # name.delete(0, 'end')
    # date.delete(0, 'end')
    song_id.delete(0, 'end')
    arousal.delete(0, 'end')
    valence.delete(0, 'end')
    positive_nostalgia.delete(0, 'end')
    nagative_nostalgia.delete(0, 'end')
    affinity.delete(0, 'end')




master = Tk()
Label(master, text="Particiant Name").grid(row=0)
Label(master, text="Date").grid(row=1)
Label(master, text="Song Id").grid(row=2)
Label(master, text="Arousal").grid(row=3)
Label(master, text="Valence").grid(row=4)
Label(master, text="Positive Nostalgia").grid(row=5)
Label(master, text="Nagative Nostalgia").grid(row=6)
Label(master, text="Affinity").grid(row=7)

name = Entry(master)
date = Entry(master)
song_id = Entry(master)
arousal = Entry(master)
valence = Entry(master)
positive_nostalgia = Entry(master)
nagative_nostalgia = Entry(master)
affinity = Entry(master)



name.grid(row=0, column=1)
date.grid(row=1, column=1)
song_id.grid(row=2, column=1)
arousal.grid(row=3, column=1)
valence.grid(row=4, column=1)
positive_nostalgia.grid(row=5, column=1)
nagative_nostalgia.grid(row=6, column=1)
affinity.grid(row=7, column=1)



Button(master, text='Quit', command=master.quit).grid(row=8, column=0, sticky=W, pady=4)
Button(master, text='Save', command=save_self_report).grid(row=8, column=2, sticky=W, pady=4)
Button(master, text='Clear', command=clean_entry).grid(row=8, column=1, sticky=W, pady=4)

mainloop( )