from Data import *


# Feature functions

def entropy_of_abs(x):
    if len(x) < 2:
        return 1e-7
    return max(1e-7, stats.entropy(x - np.min(x)))

def fl_delta(x):
    if len(x) < 2:
        return 1e-7
    res = x[-1] - x[0]
    if res == 0:
        return 1e-7
    return res

def np_1st_deriv(x):
    res = np.mean(np.gradient(x))
    if res == 0:
        return 1e-7
    return res

def np_2nd_deriv(x):
    res = np.mean(np.gradient(np.gradient(x)))
    if res == 0:
        return 1e-7
    return res

def np_1st_diff(x):
    res = np.mean(np.diff(x))
    if res == 0:
        return 1e-7
    return res

def np_2nd_diff(x):
    if len(x) < 3:
        return 1e-7
    res = np.mean(np.diff(np.diff(x)))
    if res == 0:
        return 1e-7
    return res

deriv_funcs = [
    fl_delta,
    np_1st_deriv,
    np_2nd_deriv,
    np_1st_diff,
    np_2nd_diff,
]

feature_funcs_all = [
    np.mean,
    np.std,
    np.max,
    np.min,
    np.ptp,
    entropy_of_abs,
] + deriv_funcs