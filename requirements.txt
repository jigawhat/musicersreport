numpy==1.14.5
joblib==0.12.3
pandas==0.23.4
scipy==1.1.0
scikit-learn==0.19.2
pymc3==3.6
pyGPGO==0.4.0.dev1
mlxtend==0.13.0
