#
#  Defines the final model pipeline
#  (hyperparameter optimisation, feature selection, training & testing,
#   performance & comparison graph plotting)
#


from Learning import *
from Graphing import *

# Gaussian process Bayesian optimisation package pyGPGO
from pyGPGO.covfunc import squaredExponential
from pyGPGO.acquisition import Acquisition
from pyGPGO.surrogates.GaussianProcess import GaussianProcess
from pyGPGO.surrogates.RandomForest import RandomForest
from pyGPGO.GPGO import GPGO

# Sequential forward/backward feature selection package mlxtend
from mlxtend.feature_selection import SequentialFeatureSelector as SFS

# Scikit-learn F-test and mutual information feature selection
from sklearn.feature_selection import SelectPercentile, SelectKBest, \
                                      f_classif, mutual_info_classif

import warnings
warnings.filterwarnings('ignore')


####### Full pipeline model training & testing function #######
def run_final_pipeline(
        perform_subject_included=True,# Perform subject-included prediction too
        test_music_features=True,# Test music features importances
        select_features=False,# Whether to select features or use pre-selected
        optimise_windows=False,# Optimise windows or use pre-optimised
        include_song_t=True,# Include time since song start as a window feature
        randomize_bayop_initial_x=False,# Initialize hyperparam. opt. randomly
        subject_normalize_emotions=False,# Normalize Y targets by subject
        bayop_sffs=True,# Use forward feature selection within bayesian opt
        w_configs=w_configs_default,# Initial window configurations
        sensors=sensors_default,# Sensors to use
        feature_funcs=feature_funcs_default,# Feature functions to use
        final_window_size=final_window_size_default, # Final window params
        final_window_overlap=final_window_overlap_default,
    ):

    n_sensors = len(sensors)


    ####### Load & baseline-normalize data #######


    # Load in data (sensor readings with timestamps in the first column)
    print("\n\n ######## Loading data ####### \n\n")
    data_dict = load_data(sensors=sensors)

    # Shuffle subjects and songs
    # data_dict = shuffle_data(data_dict)
    data_list = list(data_dict.values())
    print("Sensors & self report data shapes:",
        [data_list[0][s][1].shape for s in sensors + ["selfreport"]])

    # Apply baselines to data
    data_list_wb, base_labels = baseline_normalize(
        data_list, True, True, True, True, True, sensors)
    print("New shapes after baselining " + \
          "(n_readings, n_baselines + 1 (original data) + 1 (timestamp)):\n",
          [data_list_wb[0][s][1].shape for s in sensors + ["selfreport"]],
          "\nNumber of songs for each subject:\n",
          [len(data_list_wb[i]['EDA']) - 1 for i in range(len(data_list_wb))])

    # Compute binary classification emotion labels, evenly dividing
    Y_data_r,Y_data_c,Y_split_vals,max_rand_accs = even_class_div(
        data_list, subject_normalize_emotions)
    print("Y data shapes:", len(Y_data_r), [Y_data_r[i].shape for i in range(len(Y_data_r))])

    # Prepare results containers
    results = {}


    ####### Bayesian Window Hyperparameter Optimisation #######


    if optimise_windows:
        print("\n\n ######## Bayesian window hyperparameter optimisation ####### \n\n")

        # Objectives used to evaluate configuration accuracy
        bayop_objs = ["Arousal", "Valence", "Positive Nostalgia", "Affinity"]

        bayop_globalz = randomize_bayop_initial_x, sensors, data_list_wb, base_labels, \
             Y_data_r, Y_data_c, Y_split_vals, max_rand_accs, final_window_size, \
              final_window_overlap, include_song_t, feature_funcs, bayop_objs # globals for bayop function

        # Subject-independent case
        subj_ind_w_configs = deepcopy(w_configs)
        subj_ind_w_configs = window_config_bayop(
            subj_ind_w_configs, bayop_globalz, False, "subj_ind", bayop_sffs=bayop_sffs)
        # save_ld(subj_ind_w_configs, "ind_window_configs")

        print("\nOptimised subject-independent windowing configurations:")
        pr(subj_ind_w_configs)
        print()

        # Subject-included case
        if perform_subject_included:
            subj_inc_w_configs = deepcopy(w_configs)
            subj_inc_w_configs = window_config_bayop(
                subj_inc_w_configs, bayop_globalz, True, "subj_inc", bayop_sffs=bayop_sffs)
            save_ld(subj_inc_w_configs, "inc_window_configs")

            print("\nOptimised subject-included windowing configurations:")
            pr(subj_inc_w_configs)
            print()
    else:

        # Use recently optimised values
        w_configs_prev = load_ld("ind_window_configs")
        # w_configs_prev = OrderedDict([
        #     ('EDA',  [[0.6395, 0.731, 4.781],
        #               [6.8, 0.937, 1],
        #               [3.28, 0.937, 0.563],
        #               [3.27, 0.95, 4.7]]),
        #     ('BVP',  [[6.86, 0.9107, 0],
        #               [2, 0.95, 0],
        #               [5, 0.95, 0],
        #               [8, 0.9, 0]]),
        #     ('HR',   [[2, 0.95, 1],
        #               [4, 0.95, 3],
        #               [6, 0.9, 5],
        #               [8, 0.9, 7]]),
        #     ('TEMP', [[5, 0.95, 4.563],
        #               [7, 0.95, 2],
        #               [9, 0.95, 4.563],
        #               [5, 0.95, 0]]),
        # ])
        subj_ind_w_configs = w_configs_prev
        subj_inc_w_configs = w_configs_prev

    ####### Sequential Floating Forward Feature Selection #######


    if select_features:
        print("\n\n####### Sequential Floating Forward Feature Selection #######\n\n")
        # Subject independent case
        globalz = sensors, data_list_wb, base_labels, \
             Y_data_r, Y_data_c, Y_split_vals, max_rand_accs, final_window_size, \
              final_window_overlap, include_song_t, feature_funcs # globals for feature selection function

        subj_ind_feature_is, _is, ls = feature_selection(subj_ind_w_configs, globalz, False, cache_key="subj_ind")
        save_ld(subj_ind_feature_is, "ind_selected_features")
        if perform_subject_included:    
            subj_inc_feature_is, _is, ls = feature_selection(subj_inc_w_configs, globalz, True, cache_key="subj_inc")
            save_ld(subj_inc_feature_is, "inc_selected_features")


    else:
        subj_ind_feature_is = [       # subject-independent                                                  # test val acc
            [363, 124, 27, 491, 493, 457, 495, 461, 304, 489, 356, 483, 91, 332, 92, 368, 431, 90], # 81.25 71.43
            [27, 77, 1, 477, 43, 168, 42, 249, 152, 97, 98, 2, 229, 73, 110, 15, 414, 13, 47, 65],  # 73.016 93.75
            [337, 447, 252, 314, 223, 448, 80, 275, 440, 48, 102, 347, 346, 12, 446, 445, 16, 108, 222, 345], # 77.78 87.5
            [381, 49, 287, 420, 440, 283, 225, 51, 98, 52, 409, 359, 60, 412, 58, 0, 59, 50], # 66.667 37.5
            [175, 41, 9, 14, 43, 105, 13, 46, 78, 112, 400, 83, 79, 77, 51, 75, 47, 432], # 73.016 68.75
        ]
        subj_inc_feature_is = [       # subject-included                                                      # test val acc
            [267, 84, 4, 124, 579, 351, 19, 568, 100, 506, 507, 94, 44, 567, 14, 134, 54, 59],        # 76.27 60.0
            [58, 54, 379, 575, 100, 140, 535],                                                        # 79.66 70.0
            [499, 170, 391, 615, 417, 330, 564, 565, 343, 421, 525, 575, 580, 620, 0, 495, 540, 420], # 83.05 60.0
            [508, 355, 625, 240, 239, 237, 0, 510, 503, 236, 623, 624, 501, 238],                     # 71.19 65.0
            [580, 432, 565, 156, 500, 495, 535, 540, 0, 485, 620],                                    # 71.19 60.0
        ]


    ####### Final cross-validation #######


    print("\n\n####### Final cross-validation and results generation #######\n\n")
    globalz = Y_data_r, Y_data_c, Y_split_vals, max_rand_accs

    print("Windowing data for subject-independent prediction...")
    X_data_ind, X_labels_ind = windowed_features(data_list_wb, base_labels,
        final_window_size, final_window_overlap, subj_ind_w_configs,
        include_song_t, feature_funcs, sensors, cache_key="subj_ind")
    print("Windowing data for subject-included prediction...")
    X_data_inc, X_labels_inc = windowed_features(data_list_wb, base_labels,
        final_window_size, final_window_overlap, subj_inc_w_configs,
        include_song_t, feature_funcs, sensors, cache_key="subj_inc")
    save_ld((X_data_ind, X_labels_ind, X_data_inc, X_labels_inc), "windowed_res_new")
    # X_data_ind, X_labels_ind, X_data_inc, X_labels_inc = load_ld("windowed_res_new")
    # X_data_ind, X_labels_ind, X_data_inc, X_labels_inc = load_ld("windowed_res")

    # # First, compare subject-independent and subject-included
    final_selection = "sbfs"
    # final_selection = "mi"
    # final_selection = False
    ind_res, ind_feats = test_final_model(subj_ind_feature_is, False, X_data_ind, globalz, select=final_selection)
    inc_res, inc_feats = test_final_model(subj_inc_feature_is, True, X_data_inc, globalz, select=final_selection)
    ind_accs = ind_res[:, 1].flatten()
    inc_accs = inc_res[:, 1].flatten()
    save_ld((ind_accs, ind_feats, inc_accs, inc_feats, max_rand_accs), "ind_inp_comparison")
    # ind_accs, ind_feats, inc_accs, inc_feats, max_rand_accs = load_ld("ind_inp_comparison")
    plot_res_comparison(ind_accs, inc_accs, max_rand_accs)

    # Get feature importances
    print("Subject-independent feature importances:")
    ind_imps = feature_importances(ind_feats, X_data_ind, conv_ls(X_labels_ind), globalz)
    save_ld(ind_imps, "ind_fis")
    print("Subject-included feature importances:")
    inc_imps = feature_importances(inc_feats, X_data_inc, conv_ls(X_labels_inc), globalz)
    save_ld(inc_imps, "inc_fis")

    # Get feature importances

    # Test music features and their importance
    if test_music_features:
        music_feat_subj_ids = {
            5: 1,
            4: 0,
            3: 6,
            2: 3,
            1: 5,
        }
        music_feat_labels = ['energy', 'liveness', 'tempo',
           'speechiness', 'acousticness', 'instrumentalness',
           'danceability', 'key', 'loudness', 'valence']
        music_feats_db = pd.read_csv("music_features/song_feats.csv", index_col=[1,8])
        music_feats_db = music_feats_db[music_feat_labels]
        X_data_music = []
        X_data_music_c = []
        Yc_music, Yr_music = [], []
        for i in music_feat_subj_ids:
            data_i = music_feat_subj_ids[i]
            x_subj = X_data_ind[data_i]
            X_data_music_c.append(deepcopy(x_subj))
            Yr_music.append(Y_data_r[data_i])
            Yc_music.append(Y_data_c[data_i])
            for j in range(len(x_subj)):
                x_song = x_subj[j]
                new_feats = music_feats_db.loc[i, j + 1].values
                x_song = np.hstack([x_song, np.tile(new_feats, (x_song.shape[0], 1))])
                x_subj[j] = x_song
            X_data_music.append(x_subj)
        X_labels_music = X_labels_ind + ["musicFeature_" + x for x in music_feat_labels]
        music_feat_is = []
        for emo_i in range(len(emo_labels_all)):
            music_feat_is.append(subj_ind_feature_is[emo_i] + list(range(len(X_labels_ind), len(X_labels_music))))
        globalz = Yr_music, Yc_music, Y_split_vals, max_rand_accs

        # Train model with additional features
        selection = "sbfs"
        # selection = 'mi'
        msc_res, msc_feats = test_final_model(music_feat_is, False, X_data_music, globalz,
            select=selection, select_k=max_features_default + len(music_feat_labels))
        # Train original model with the same dataset
        reg_res, reg_feats = test_final_model(subj_ind_feature_is, False, X_data_music_c, globalz, select=selection)
        msc_accs = msc_res[:, 1].flatten()
        reg_accs = reg_res[:, 1].flatten()
        save_ld((reg_accs, reg_feats, msc_accs, msc_feats, max_rand_accs), "msc_reg_comparison")
        # reg_accs, reg_feats, msc_accs, msc_feats, max_rand_accs = load_ld("msc_reg_comparison")
        plot_res_comparison(reg_accs, msc_accs, max_rand_accs, filename="music",
            l1="Physiological features", l2="Physiological+Musical features")

        # Get feature importances
        print("Musical feature importances:")
        msc_imps = feature_importances(msc_feats, X_data_music, conv_ls(X_labels_music), globalz)
        save_ld(msc_imps, "msc_fis")
        print("Physiological feature importances:")
        reg_imps = feature_importances(reg_feats, X_data_music_c, conv_ls(X_labels_ind), globalz)
        save_ld(reg_imps, "reg_fis")

    # Also, plot postitive nostalgia and affinity on arousal valence graph

def feature_importances(feature_is, X_data, X_labels_formd, globalz, select=False):
    Y_data_r, Y_data_c, Y_split_vals, max_rand_accs = globalz
    emos = emo_labels_all
    n_emos = len(emos)
    n_samples = 1
    r = {}
    emo_is = [emo_labels_all.index(x) for x in emos]
    feat_accuracies = []
    for i in range(len(emos)):
        all_feat_is, emo = feature_is[i], emos[i]
        X_d = X_data
        X_d = [[x_[:,all_feat_is] for x_ in x] for x in X_data]
        data_tup = X_data, Y_data_r, Y_data_c, Y_split_vals, "SVC", "SVR"
        res=Parallel(n_jobs=1)(delayed(test_model)(data_tup, False, [emo],
            select=select) for j in range(n_samples))
        train_accs, test_accs, val_accs = [np.mean(accs, axis=0) for accs in zip(*res) if isinstance(accs[0], np.ndarray)]
        scaler, selector, m, _tra, _tsa, _va = res[-1]
        train_success,test_success,val_success=[
            accs-(100*np.array(max_rand_accs)[emo_is]) for accs in [train_accs,test_accs,val_accs]]
        r[emo] = [test_accs[0], test_success[0], val_accs[0], val_success[0]]
        best_acc = test_accs[0]
        # print("Best acc", best_acc)

        feat_accs = [best_acc]
        for j in range(len(all_feat_is)):
            feat_is = [all_feat_is[k] for k in range(len(all_feat_is)) if k != j]
            X_d = X_data
            X_d = [[x_[:, feat_is] for x_ in x] for x in X_data]
            data_tup = X_d, Y_data_r, Y_data_c, Y_split_vals, "SVC", "SVR"
            res=Parallel(n_jobs=n_cpu)(delayed(test_model)(data_tup, False, [emo],
            select=select) for j in range(n_samples))
            train_accs, test_accs, val_accs = [np.mean(accs, axis=0) for accs in zip(*res) if isinstance(accs[0], np.ndarray)]
            feat_accs.append(test_accs[0])
        feat_accuracies.append(np.asarray(feat_accs))

    rankings = []
    for i in range(len(emos)):
        print("\nRanking for", emos[i])
        emo = emos[i]
        feat_is = feature_is[i]
        feat_accs = feat_accuracies[i]
        best_acc = feat_accs[0]
        rest_accs = feat_accs[1:]
        feat_loss = best_acc - rest_accs
        ranking = sorted([[feat_loss[j], X_labels_formd[feat_is[j]]] for j in range(len(feat_is))])
        for j in range(len(ranking)):
            ranking[j][0] = round(ranking[j][0], 2)
    #     ranking = ranking[:10]
        pr(ranking)
        rankings.append(ranking)

    return rankings

def conv_ls(ls):
    return [convert_label(l) for l in ls]

def label_format(l):
    conv = []
    for i in range(len(l)):
        o = ord(l[i])
        if o >= 65 and o <= 90:
            conv += [i]
    convd = 0
    for i in conv:
        letter = chr(ord(l[i + convd]) + 0)
        l = l[:i + convd] + ' ' + letter + l[-(len(l) - i - 1 - convd):]
        convd += 1
    l = l[0].upper() + l[1:]
    return l

def convert_label(x):
    elems = x.split('_')
    if len(elems) > 2:
        elems[2] = "Window " + elems[2][-1]
        if elems[3] == "amin":
            elems[3] = "min"
        elif elems[3] == "amax":
            elems[3] = "max"
        elif elems[3] == "ptp":
            elems[3] = "peak to peak"
        elif elems[3] == "std":
            elems[3] = "standard deviation"
        elif elems[3] == "np":
            elems[3] = ''
            if elems[5] == "deriv":
                elems[5] = "derivative"
        elif elems[3] == 'fl':
            elems[3] = 'window'
            elems[3] = "max"
    elif len(elems) > 1:
        elems[1] = elems[1][0].upper() + elems[1][1:]
    elems[0] = label_format(elems[0])
    return ' '.join(elems)


def test_final_model(feature_is, subj_inc, X_data, globalz, select=False, select_k=max_features_default):
    Y_data_r, Y_data_c, Y_split_vals, max_rand_accs = globalz
    r, emo_i, n_samples = [], 0, 1
    best_features = []
    for emo in emo_labels_all:
        X_data_ = [[x_[:, feature_is[emo_i]] for x_ in x] for x in X_data]
        data_tup = X_data_, Y_data_r, Y_data_c, Y_split_vals, "SVC", "SVR"
        res = Parallel(n_jobs=n_cpu)(delayed(test_model)(
            data_tup, subj_inc, [emo], select=select, select_k=select_k) for i in range(n_samples))
        train_accs, test_accs, val_accs = [
            np.mean(accs, axis=0) for accs in zip(*res) if isinstance(accs[0], np.ndarray)]
        scaler, selector, m, _tra, _tsa, _va = res[-1]
        train_success, test_success, val_success = [
            accs - (100 * max_rand_accs[emo_i]) for accs in [train_accs, test_accs, val_accs]]
        train_acc_score = round(train_accs[-1], 3)
        test_acc_score = round(test_accs[-1], 3)
        val_acc_score = round(val_accs[-1], 3)
        train_score = round(train_success[-1], 3)
        test_score = round(test_success[-1], 3)
        val_score = round(val_success[-1], 3)
        r.append([train_acc_score, test_acc_score, val_acc_score])
        if select == "sffs" or select == "sbfs":
            best_features.append([feature_is[emo_i][i] for i in selector.k_feature_idx_])
        elif select:
            best_features.append([feature_is[emo_i][i] for i in range(5)])
    return np.asarray(r), best_features

def plot_res_comparison(ind_data, inc_data, max_rand_accs, filename="indp",l1="Subject-independent",l2="Subject-included"):
    print(ind_data, inc_data, max_rand_accs)
    emos = emo_labels_all
    emo_ls = [e.replace(' ', '\n') for e in emos]
    ax = plt.subplot(111)
    # fig = plt.gcf()
    # fig.clear()
    b1 = ax.bar(np.arange(len(emos)), inc_data, width=0.3,color='b',align='center', tick_label=emo_ls)
    b2 = ax.bar(np.arange(len(emos)) + 0.3, ind_data, width=0.3,color='r',align='center')
    b3 = ax.bar(np.arange(len(emos))+0.15, np.asarray(max_rand_accs) * 100, width=0.6,alpha=0.4,color='c',align='center')
    # ax.bar(x, z,width=0.2,color='g',align='center')
    # ax.bar(x+0.2, k,width=0.2,color='r',align='center')
    # ax.xlabel()
    plt.ylim((50, 92))
    ax.set_axisbelow(True)
    ax.grid()
    ax.set_ylabel("Cross Validation Test Accuracy %")
    # ax.set_title

    ax.legend((b1[0], b2[0], b3[0]), (l2, l1, 'Random Guessing\n(Class ratios)'))
    def autolabel(rects):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            alpha = rect.get_alpha()
            light = False
            if alpha is not None and alpha < 1.0:
                light = True
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                    str(round(height, 1)),
                    ha='center', va='bottom', color='c' if light else 'black')

    autolabel(b1)
    autolabel(b2)
    autolabel(b3)

    fig = plt.gcf()
    fig.set_size_inches(8, 5)

    plt.savefig(graphics_dir + "/" + filename + ".pdf", filetype='pdf')
    plt.show()


# Sequential floating forward selection function
def feature_selection(w_configs, globalz, subj_inc, labels=None, cache_key="default"):
    sensors, data_list_wb, base_labels, \
        Y_data_r, Y_data_c, Y_split_vals, max_rand_accs, final_window_size,\
         final_window_overlap, include_song_t, feature_funcs = globalz

    # Get windowed data
    print("Windowing data...")
    X_data, X_labels = windowed_features(data_list_wb, base_labels,
                    final_window_size, final_window_overlap, w_configs,
                    include_song_t, feature_funcs, sensors, cache_key="subj_inc" if subj_inc else "subj_ind")

    # If subject-independent, remove subjectSongBaseline features
    final_feature_is = list(range(len(X_labels)))
    final_X_labels = X_labels
    if not subj_inc:
        new_feature_is, new_X_labels = [], []
        labels_is = list(range(len(X_labels)))
        for i in final_feature_is:
            if "subjectSongBaseline" not in X_labels[i]:
                new_feature_is.append(i)
                new_X_labels.append(X_labels[i])
        final_feature_is, final_X_labels = new_feature_is, new_X_labels

    # Get best features for each emotion and subject-dependency case
    full_res = defaultdict(list)
    r, r_scores = {}, {}
    emo_labels = emo_labels_all
    emo_is = [emo_labels_all.index(x) for x in emo_labels]
    for emo in emo_labels:
        print()
        print("Feature selection starting for", emo, "(subject-" + ("included" if subj_inc else "independent") + ")...")
        print()
        best_score = -float('inf')
        for repeat_i in range(5 - len(full_res[emo])):

            print("Algorithm restart #", repeat_i, "for emotion", emo)

            # Shuffle features
            feature_is = deepcopy(final_feature_is)
            np.random.shuffle(feature_is)
            X_d = [[x_[:, feature_is] for x_ in x] for x in X_data]
            
            # Test model
            data_tup = X_d, Y_data_r, Y_data_c, Y_split_vals, "SVC", "SVR"
            res = Parallel(n_jobs=1)(delayed(test_model)(
                data_tup, subj_inc, [emo], validate=True) for i in range(5))

            # Get performance and selected features
            train_accs, test_accs, val_accs = [
                np.mean(accs, axis=0) for accs in zip(*res) if isinstance(accs[0], np.ndarray)]
            scaler, selector, m, _tra, _tsa, _va = res[-1]
            train_success, test_success, val_success = [
                accs - (100 * np.array(max_rand_accs)[emo_is]) for accs in [train_accs, test_accs, val_accs]]
            train_acc_score = round(train_accs[-1], 3)
            test_acc_score = round(test_accs[-1], 3)
            val_acc_score = round(val_accs[-1], 3)
            train_score = round(train_success[-1], 3)
            test_score = round(test_success[-1], 3)
            val_score = round(val_success[-1], 3)
            selected_features = [feature_is[i] for i in selector.k_feature_idx_]

            # Append result
            full_res[emo].append((train_acc_score, train_score, test_acc_score,
                test_score, val_acc_score, val_score, selected_features))
            for res_ in full_res[emo]:
                print(res_)
            if val_score > best_score:
                print("New best validation accuracy for " + emo + \
                    " [" +"subject-" + ("included" if subj_inc else "independent") + "] !")
                r[emo] = res
                r_scores[emo] = (test_acc_score, test_score, val_acc_score, val_score, selected_features)
                best_score = val_score
            print("Current best for " + emo + " [" + \
                "subject-" + ("included" if subj_inc else "independent") + "]:")
            print(r_scores[emo])

    print()
    print(full_res)
    print()

    # Get top performing feature sets
    res = OrderedDict()
    for emo in emo_labels:
        val_scores = np.asarray([x[5] for x in full_res[emo]])
        max_score = max(val_scores)
        maxed_is = np.nonzero(val_scores == max_score)[0]
        if len(maxed_is == 1):
            best_i = maxed_is[0]
        else:
            maxed_test_scores = np.asarray([full_res[emo][i][3] for i in maxed_is])
            best_i = maxed_is[np.argmax(maxed_test_scores)]
        best_features = full_res[emo][best_i][-1]
        res[emo] = best_features
        print("Best features for", emo, ":", full_res[emo][best_i][2:])
    print(res)
    res = list(res.values())
    return res, final_feature_is, final_X_labels


def parallel_model_test(*params, n_repeat_samples=1, n_jobs=n_cpu, shuffle_features=True):
        res = Parallel(n_jobs=n_jobs)(delayed(
            test_model)(*params, shuffle_features=shuffle_features) for i in range(n_repeat_samples))
        train_accs, test_accs, val_accs = [np.mean(
            accs, axis=0) for accs in zip(*res) if isinstance(accs[0], np.ndarray)]
        scaler, selector, m, _tra, _tsa, _va = res[-1]
        return scaler, selector, m, train_accs, test_accs, val_accs

def window_config_bayop(w_configs, globalz, subj_incl=False, cache_key="default", bayop_sffs=False):
    randomize_bayop_initial_x, sensors, data_list_wb, base_labels, \
        Y_data_r, Y_data_c, Y_split_vals, max_rand_accs, final_window_size,\
         final_window_overlap, include_song_t, feature_funcs, bayop_objs = globalz


    # If reinitializing starting values randomly, build the windows up
    # sequentially starting from one configuration per sensor
    if randomize_bayop_initial_x:
        for s in sensors:
            w_configs[s] = w_configs[s][:1]
            # w_configs[s][0] = random_window_conf(sensors.index(s)) # But start first 4 from something reasonable
            if s == "BVP":
                w_configs[s][0][2] = 0

    def bayop_test_model(all_data_tup, subj_incl, emos, n_jobs=n_cpu):
        n_repeat_samples = 1
        succs = []
        for emo in emos:
            emo_i = emo_labels_all.index(emo)

            # Floating forward selection
            if bayop_sffs:
                _a, _b, _c, _d, test_accs, _e = parallel_model_test(
                    all_data_tup, subj_incl, [emo], 'sffs', 4, n_jobs=n_jobs)
                test_success = test_accs[0] - (100 * max_rand_accs[emo_i])
                succs.append(test_success)
            else: # F-test and mutual information feature selection
                _a, _b, _c, _d, test_accs, _e = parallel_model_test(
                    all_data_tup, subj_incl, [emo], 'ft', n_jobs=n_jobs)
                test_success_ft = test_accs[0] - (100 * max_rand_accs[emo_i])
                _a, _b, _c, _d, test_accs, _e = parallel_model_test(
                    all_data_tup, subj_incl, [emo], 'mi', n_jobs=n_jobs)
                test_success_mi = test_accs[0] - (100 * max_rand_accs[emo_i])
                succs.append(max(0, max(test_success_ft, test_success_mi)))
        return np.asarray(succs)

    # For each sensor, optimise each window configuration
    print("Initial windowing configurations:")
    pr(w_configs)
    print()
    for w_i in range(n_windows_per_sensor):
        for s_i in range(len(sensors)):
            s = sensors[s_i]
            print("Optimising window", w_i + 1, "for", s)
            if len(w_configs[s]) < w_i + 1:
                w_configs[s].append(random_window_conf(s_i))
            elif randomize_bayop_initial_x and w_i == 0:
                w_configs[s][0] = random_window_conf(s_i)

            def f(**x):
                x = [x['x' + str(i)] for i in range(len(x))]
                if len(x) == 2:
                    x = np.array([x[0], x[1], 0.])
                conf = deepcopy(w_configs)
                for s_ in sensors:
                    if s_ == s:
                        conf[s][w_i] = scale_window_conf(x, s_i)
                    # else:
                    #     conf[s] = conf[s][:1]
                X_d, X_l = windowed_features(data_list_wb, base_labels,
                    final_window_size, final_window_overlap, conf,
                    include_song_t, feature_funcs, sensors, cache_key=cache_key)
                all_data_tup=X_d,Y_data_r,Y_data_c,Y_split_vals,"SVC","SVR"
                res = bayop_test_model(all_data_tup, subj_incl, bayop_objs, n_jobs=1)
                print(s, res, x)
                return (np.mean(res) + ((np.random.random() - 0.5) / 5)) / 100

            kern = squaredExponential()
            gp = GaussianProcess(kern, optimize=True)
            # gp = RandomForest()
            acq = Acquisition(mode='ProbabilityImprovement')
            param = {'x' + str(i): ('cont', [0, 1]) for i in range(2 if (w_i == 0 and s == "BVP") else 3)}

            np.random.seed()
            # np.random.seed(23)
            gpgo = GPGO(gp, acq, f, param, n_jobs=1)
            gpgo.run(max_iter=20, init_evals=3, resume=False)
            new_conf, new_conf_acc = gpgo.getResult()
            vals = list(new_conf.values())
            if len(vals) < 3:
                vals.append(0.)
            new_conf = scale_window_conf(np.asarray(vals), s_i)
            print("New", s, "config:", new_conf)
            w_configs[s][w_i] = new_conf

    return w_configs


# Test model function
# If rc true, perform regression instead and label based on the regression prediction
def test_model(data_tup, subj_inc=False, emos=emo_labels_all, select="sffs", select_k=max_features_default,
               rc=False, fade_in_buffer=1, always_incl_t=False, validate=False, shuffle_features=False,
               max_features=max_features_default, min_features=min_features_default):
    e_i = [emo_labels_all.index(i) for i in emos]
    X_data, Yr_data, Yc_data, Y_split_vals, model_c, model_r = deepcopy(data_tup)
    train_accuracies, test_accuracies = [], []
    n_features = X_data[0][0].shape[1]

    if shuffle_features:
        feature_is = list(range(X_data[0][0].shape[1]))
        np.random.shuffle(feature_is)
        X_data = [[x_[:, feature_is] for x_ in x] for x in X_data]
    
    # Filter out uneeded Y data
    Yr_data = [y[:, e_i] for y in Yr_data]
    Yc_data = [y[:, e_i] for y in Yc_data]
    Y_split_vals = Y_split_vals[e_i]

    # Fade in buffer (ignore some number of windows at the beginning of each song)
    X_data = [[x_[fade_in_buffer:] for x_ in x] for x in X_data]

    # Shuffle song windows, songs, and subjects
    for i in range(len(X_data)):
        for j in range(len(X_data[i])):
            x_song = X_data[i][j]
            np.random.shuffle(x_song) # Always shuffle windows
            X_data[i][j] = x_song
        if (not subj_inc) or not validate: # Only shuffle songs if we don't need them ordered
            song_inds = list(range(len(X_data[i])))
            np.random.shuffle(song_inds)
            X_data[i] = [X_data[i][j] for j in song_inds]
            Yr_data[i] = Yr_data[i][song_inds]
            Yc_data[i] = Yc_data[i][song_inds]
    if not validate: # Only shuffle subjects if we don't nee them ordered
        subj_inds = list(range(len(X_data)))
        np.random.shuffle(subj_inds)
        X_data = [X_data[j] for j in subj_inds]
        Yr_data = [Yr_data[j] for j in subj_inds]
        Yc_data = [Yc_data[j] for j in subj_inds]
    
    # Swap data for random garbage to test overfitting
#     X_data = [[np.random.random(x_[fade_in_buffer:].shape) for x_ in x] for x in X_data]
    
    # Get validation data
    if validate:
        if not subj_inc: # Keep out subject(s) for validation
            n_val_subjs = 2  # Number of subjects to keep data out for validation (20%)
            X_data_val = [x for x in X_data[-n_val_subjs:]]
            X_data_tr = [x for x in X_data[:-n_val_subjs]]
            Yr_data_val = [y for y in Yr_data[-n_val_subjs:]]
            Yr_data_tr = [y for y in Yr_data[:-n_val_subjs]]
            Yc_data_val = [y for y in Yc_data[-n_val_subjs:]]
            Yc_data_tr = [y for y in Yc_data[:-n_val_subjs]]

        else: # Keep out songs for validation
            n_val_songs = 2 # Number of songs from each subject to keep out (25%)

            X_data_val = [x[-n_val_songs:] for x in X_data]
            X_data_tr = [x[:-n_val_songs] for x in X_data]
            Yr_data_val = [x[-n_val_songs:] for x in Yr_data]
            Yr_data_tr = [x[:-n_val_songs] for x in Yr_data]
            Yc_data_val = [x[-n_val_songs:] for x in Yc_data]
            Yc_data_tr = [x[:-n_val_songs] for x in Yc_data]
    else:
        X_data_val = X_data
        X_data_tr = X_data
        Yr_data_val = Yr_data
        Yr_data_tr = Yr_data
        Yc_data_val = Yc_data
        Yc_data_tr = Yc_data
    
    # Collect data for each song
#     X_songs = sum(X_data, [])
#     Yr_songs = np.vstack(Yr_data)
#     Yc_songs = np.vstack(Yc_data)
    X_songs_tr = sum(X_data_tr, [])
    Yr_songs_tr = np.vstack(Yr_data_tr)
    Yc_songs_tr = np.vstack(Yc_data_tr)
    X_songs_val = sum(X_data_val, [])
    Yr_songs_val = np.vstack(Yr_data_val)
    Yc_songs_val = np.vstack(Yc_data_val)
    n_classes = np.asarray([np.unique(Yc_songs_tr[:, i]).shape[0] for i in range(len(emos))])
    
    # And for each window
#     X = np.vstack(X_songs)
#     Yr = np.vstack([np.tile(Yr_songs[i], (len(X_songs[i]), 1)) for i in range(len(X_songs))])
#     Yc = np.vstack([np.tile(Yc_songs[i], (len(X_songs[i]), 1)) for i in range(len(X_songs))])
    X_tr = np.vstack(X_songs_tr)
    Yr_tr = np.vstack([np.tile(Yr_songs_tr[i], (len(X_songs_tr[i]), 1)) for i in range(len(X_songs_tr))])
    Yc_tr = np.vstack([np.tile(Yc_songs_tr[i], (len(X_songs_tr[i]), 1)) for i in range(len(X_songs_tr))])
    X_val = np.vstack(X_songs_val)
    Yr_val = np.vstack([np.tile(Yr_songs_val[i], (len(X_songs_val[i]), 1)) for i in range(len(X_songs_val))])
    Yc_val = np.vstack([np.tile(Yc_songs_val[i], (len(X_songs_val[i]), 1)) for i in range(len(X_songs_val))])
    Y_tr = Yr_tr if rc else Yc_tr
    
    # Label each window with it's song and subject group
#     song_groups = np.hstack([np.repeat(i, len(X_songs[i])) for i in range(len(X_songs))])
#     subj_groups = np.hstack([
#         np.repeat(i, sum([len(X_data[i][j]) for j in range(len(X_data[i]))])) for i in range(len(X_data))])
    song_groups_tr = np.hstack([np.repeat(i, len(X_songs_tr[i])) for i in range(len(X_songs_tr))])
    subj_groups_tr = np.hstack([
        np.repeat(i, sum([len(X_data_tr[i][j]) for j in range(len(X_data_tr[i]))])) for i in range(len(X_data_tr))])
    song_groups_val = np.hstack([np.repeat(i, len(X_songs_val[i])) for i in range(len(X_songs_val))])

    # Split into training & test datasets
    cv_model, cv_selection, groups = 10, 5, None
    if subj_inc: # Subject dependent prediction
        # Leave one song group out cross validation
#         cv_model = LeavePGroupsOut(n_groups=8)
#         cv_model = LeaveOneGroupOut()
        cv_model = GroupKFold(n_splits=10)
#         cv_model = StratifiedKFold(n_splits=10)
#         cv_selection = LeavePGroupsOut(n_groups=8) # Leave out a group of songs for feature selection cv
#         cv_selection = LeaveOneGroupOut()
        cv_selection = GroupKFold(n_splits=10)
#         cv_selection = StratifiedKFold(n_splits=10)
        groups = song_groups_tr
    
    else: # Subject independent prediction  
        # Leave one subject out cross validation
        cv_model = LeaveOneGroupOut()
        cv_selection = LeaveOneGroupOut()
#         cv_selection = StratifiedKFold(n_splits=5)
        groups = subj_groups_tr

    # Define feature standardization, selection and model
    scaler = RobustScaler()  # Standardization    
    X_tr = scaler.fit_transform(X_tr)

#     m = get_model(model_r if rc else model_c)  # Model
    m = MultiOutputClassifier(SVC(decision_function_shape='ovo'))

    X_tr_ = X_tr
    if select:  # Feature selection
        max_feats = min(n_features, select_k)
        min_feats = min(min_features, max_feats - 1)
        cv_selection = list(cv_selection.split(X_tr, Y_tr, groups))
        if select == True or select == "sffs":
            selector = SFS(estimator=m,
                   k_features=(min_feats, max_feats),
                   forward=True, 
                   floating=True, 
    #                scoring='accuracy',
                   cv=cv_selection,
                   # groups=groups,
                   n_jobs=n_cpu,
                   verbose=2)
        elif select == "sbfs":
            selector = SFS(estimator=m,
                   k_features=(min_feats, max_feats),
                   forward=False, 
                   floating=True, 
    #                scoring='accuracy',
                   cv=cv_selection,
                   # groups=groups,
                   n_jobs=n_cpu,
                   verbose=2)
        elif select == "mi":
            selector = SelectKBest(k=max_feats, score_func=mutual_info_classif)
        elif select == "ft":
            selector = SelectKBest(k=max_feats, score_func=f_classif)
#         selector = SelectKBest(k=20, score_func=f_classif)
#         selector = SelectPercentile(percentile=7, score_func=f_classif)

        X_tr_ = selector.fit_transform(X_tr, Y_tr)
    
    if always_incl_t:
        X_tr_ = np.hstack([X_tr[:, :1], X_tr_])
    X_tr = X_tr_
    
    # Train & predict
    tr_is = None
    preds_train, preds_test = np.zeros(Y_tr.shape), np.zeros(Y_tr.shape)
    for train_is, test_is in cv_model.split(X_tr, Y_tr, groups=groups):
        tr_is = train_is
        np.random.shuffle(train_is)
        X_train = X_tr[train_is]
        
        # Fit model for fold
        m.fit(X_train, Y_tr[train_is])
        
        # Predict for fold train and test set
        preds_train[train_is] = m.predict(X_train)
        preds_test[test_is] = m.predict(X_tr[test_is])
    
    m.fit(X_tr, Y_tr)
    X_val_scld = scaler.transform(X_val)
    X_val_test = np.hstack(([X_val[:, :1]] if always_incl_t else []) + [
        (selector.transform(X_val_scld) if select else X_val_scld)])
    preds_val = m.predict(X_val_test)
    
    # Get predictions for each song
    if rc:
        preds_train = (preds_train > Y_split_vals).astype(float)
        preds_test = (preds_test > Y_split_vals).astype(float)
        preds_val = (preds_val > Y_split_vals).astype(float)
    song_preds_train, song_preds_test = [], []
    for i in range(len(X_songs_tr)):
        indices = np.nonzero(song_groups_tr == i)[0] 
        song_preds_train.append(np.argmax(np.asarray([[sum(
            preds_train[indices, k] == j) for j in range(n_classes[k])] for k in range(len(emos))]), axis=1))
        song_preds_test.append(np.argmax(np.asarray([[sum(
            preds_test[indices, k] == j) for j in range(n_classes[k])] for k in range(len(emos))]), axis=1))
    song_preds_train = np.vstack(song_preds_train)
    song_preds_test = np.vstack(song_preds_test)
    song_preds_val = []
    for i in range(len(X_songs_val)):
        indices = np.nonzero(song_groups_val == i)[0] 
        song_preds_val.append(np.argmax(np.asarray([[sum(
            preds_val[indices, k] == j) for j in range(n_classes[k])] for k in range(len(emos))]), axis=1))
    song_preds_val = np.vstack(song_preds_val)
    
#     train_sizes_abs, train_scores, test_scores = learning_curve(m, X, Y, )

    # Get classification accuracy
    train_accuracies = np.mean(Yc_songs_tr == song_preds_train, axis=0) * 100
    test_accuracies = np.mean(Yc_songs_tr == song_preds_test, axis=0) * 100
    val_accuracies = np.mean(Yc_songs_val == song_preds_val, axis=0) * 100
    return scaler, selector if select else None, m, train_accuracies, test_accuracies, val_accuracies


