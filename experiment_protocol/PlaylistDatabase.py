import sqlite3 as sq3
import pandas.io.sql as pds
import pandas as pd
import numpy as np
import datetime as dt
import os


DATABASE_DIR_NAME = "./"
DATABASE_FILE_NAME = "Playlists.db"
DATABASE_FULL_NAME = os.path.join(DATABASE_DIR_NAME, DATABASE_FILE_NAME)

SONGS_PER_QUESTION = 3

class PlaylistDatabase(object):

    def __init__(self):
        self.conn = sq3.connect(DATABASE_FULL_NAME)


    def get_subject_id(self, given_name, family_name):
        sql = "SELECT SubjectID FROM Subjects WHERE GivenName='" + given_name + "' AND FamilyName = '" + family_name + "'"
        res = pds.read_sql(sql, self.conn)
        if res.size == 0:
            subject_id = None
        else:
            assert res.size==1, 'Duplicate subject_id?'
            subject_id = res.iloc[0]['SubjectID']

        return subject_id

    def get_subjects(self):
        sql = "SELECT * FROM Subjects"
        res = pds.read_sql(sql, self.conn)
        return res

    def check_subject_id(self, subject_id):
        sql = "SELECT SubjectID FROM Subjects WHERE SubjectID = " + str(subject_id)
        res = pds.read_sql(sql, self.conn)
        assert len(res) == 1, "subject_id " + str(subject_id) + " doesn't exist in Subjects table"

    def get_questions(self):
        sql = "SELECT * FROM SurveyQuestions"
        res = pds.read_sql(sql, self.conn)
        return res

    def get_songs(self, subject_id=None):
        if subject_id is None:
            sql = "SELECT * FROM Songs"
        else:
            sql = "SELECT * FROM Songs where SubjectID = " + str(subject_id)

        res = pds.read_sql(sql, self.conn)
        return res

    def delete_songs(self, subject_id):
        self.check_subject_id(subject_id)
        sql = "DELETE FROM Songs WHERE SubjectID = " + str(subject_id)
        self.conn.execute(sql)
        self.conn.commit()

    def add_new_subject(self, given_name, family_name):
        subject_id = self.get_subject_id(given_name, family_name)

        assert subject_id is None, 'Subject already exists'

        sql = "SELECT max(SubjectID) FROM Subjects"
        res = pds.read_sql(sql, self.conn)
        subject_id = res['max(SubjectID)'][0] + 1

        sql = "INSERT INTO Subjects VALUES (" + str(subject_id) + ", '" + given_name + "', '" + family_name + "', NULL)"
        self.conn.execute(sql)
        self.conn.commit()

        return subject_id

    def add_blank_songs_for_subject(self, subject_id):
        self.check_subject_id(subject_id)

        sql = "SELECT * FROM Songs WHERE SubjectID=" + str(subject_id)
        res = pds.read_sql(sql, self.conn)

        assert res.size==0, 'Songs already exists for subject'

        questions = self.get_questions()
        question_ids = list(questions['QuestionID'])
        n_questions = len(question_ids)

        for q in question_ids:
            for i in range(SONGS_PER_QUESTION):
                sql = "INSERT INTO Songs VALUES (" + str(subject_id) + ", '" + q + "', " + str(i+1) + ", NULL, NULL, NULL, 0, NULL, NULL)"
                self.conn.execute(sql)

        self.conn.commit()

    def get_playlist_for_subject_id(self, subject_id):
        songs = self.get_songs(subject_id)
        songs_to_play = songs[songs['PlaylistOrder'] > 0]
        songs_to_play = songs_to_play.sort_values('PlaylistOrder')
        #assert songs_to_play['PlaylistOrder'].sum() == songs_to_play['PlaylistOrder'].max() \
        #                                               * (songs_to_play['PlaylistOrder'].max() + 1) / 2, "Check song order: there are gaps or duplicates"

        return songs_to_play

    def write_playlist_to_csv(self, subject_id):
        songs_to_play = self.get_playlist_for_subject_id(subject_id)

        subjects = self.get_subjects()
        this_subject = subjects[subjects['SubjectID'] == subject_id]

        file_name = this_subject['GivenName'].values[0] + this_subject['FamilyName'].values[0] + ".xlsx"
        print(file_name)
        songs_to_play.to_excel(excel_writer=file_name, index=False, columns=('Title', 'SpotifyURI', 'StartTimeSecs'))

        file_name = this_subject['GivenName'].values[0] + this_subject['FamilyName'].values[0] + ".csv"
        songs_to_play.to_csv(path_or_buf=file_name, index=False, columns=('Title', 'SpotifyURI', 'StartTimeSecs'))


