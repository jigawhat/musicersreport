import spotipy
import spotipy.util as util
import time
import sys
import pandas as pd
import argparse

FLAGS = None

SPOTIPY_CLIENT_ID='ec8c2025129f45f79924e7ce4973f980'
SPOTIPY_CLIENT_SECRET='f2647feddc2249d8b93df448d792caef'
SPOTIPY_REDIRECT_URI='http://localhost/'

scope = 'user-library-read user-modify-playback-state'
username = 'richard'

token = util.prompt_for_user_token(username,
                                   scope,
                                   client_id=SPOTIPY_CLIENT_ID,
                                   client_secret=SPOTIPY_CLIENT_SECRET,
                                   redirect_uri='http://localhost/')


def countdown(t):
    while t:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(timeformat, end='\r')
        time.sleep(1)
        t -= 1
    timeformat = '{:02d}:{:02d}'.format(0, 0)
    print(timeformat, end='\r')
    print('\n')
    #print('Goodbye!\n\n\n\n\n')


def main():
    songs_df = pd.read_excel(FLAGS.playlist_file, sheetname=FLAGS.subject, index_col=0)
    print('\n')
    print(songs_df)
    print('\n')
    songs = songs_df.to_dict(orient='index')

    # create spotify connection
    spotify = spotipy.Spotify(token)

    # stop Spotify (if already playing)
    try:
        spotify.pause_playback()
    except:
        pass

    # play samples of all songs in the playlist
    for song in songs:
        print(song + ': ' + songs[song]['SpotifyURI'])
        print(time.strftime('%X %x %Z'))

        # start track
        spotify.start_playback(uris=[songs[song]['SpotifyURI']])

        # move to start point
        spotify.seek_track(position_ms=songs[song]['StartTimeSecs'] * 1000)

        # set full volume
        spotify.volume(100)

        # countdown the song sample period
        countdown(FLAGS.song_sample_secs)

        # break for silence between track
        spotify.pause_playback()
        print('Take a break...')
        countdown(FLAGS.silence_secs)

        # spotify.pause_playback()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
      '--playlist_file',
      type=str,
      default='playlists.xlsx',
      help='Playlist spreadsheet.')
    parser.add_argument(
      '--subject',
      type=str,
      default='Subject1',
      help='Which sheet to take from the spreadsheet file.')
    parser.add_argument(
      '--song_sample_secs',
      type=int,
      default=60*2,
      help='How many seconds of each song to play.')
    parser.add_argument(
      '--silence_secs',
      type=int,
      default=30,
      help='How many seconds to wait between songs.')

    FLAGS, unparsed = parser.parse_known_args()
    main()
