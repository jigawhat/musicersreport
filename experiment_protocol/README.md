# Play music samples

### Dependencies
* Spotify Premium
* Spotipy: https://github.com/plamere/spotipy

### Playlist Configuration
* Add a new sheet to 'playlists.xlsx for each subject (i.e. person.)
* List their songs in a table (use Subject1 as a template.)
* Take the Spotify track URIs from the Spotify application (right click on the song -> Share -> Copy Spotify URI)
* Work out a suitable start point in seconds using the Spotify application


### Command Line Call
```python
python play_playlist.py --song_sample_secs=10 --silence_secs=5 --subject=Subject1
```
