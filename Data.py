'''

    This file contains methods & constants for data processing

'''

from Constants import *
from Utils import *

from scipy import stats


# Preprocess song data when loading in
def preprocess_song_data(d, s, acceleration_magnitude):
    # Make timestamps relative to song/baseline start
    d[:, 0] = d[:, 0] - min(d[:, 0])

    # Combine acceleration dimensions into one magnitude value
    if acceleration_magnitude and s == "ACC":
        d = np.vstack([d[:, 0].reshape(-1),
            stats.gmean(d[:, 1:]**2, axis=1)]).T
    return d

# Get data dictionary
# { participant name dict ->
#       sensor dict ->
#           song dict ("baseline" or number from 1 to 8)
#               -> data}
def load_data(acceleration_magnitude=True, sensors=sensors_all):

    empatica_dir = os.path.join(data_dir, empatica_data_dir)
    selfreport_dir = os.path.join(data_dir, selfreport_data_dir)

    res = OrderedDict()
    participants = list(participants_all)
    # np.random.shuffle(participants)
    for p in participants:
        p_emp_dir = os.path.join(empatica_dir, p)
        p_sr_dir = os.path.join(selfreport_dir, p)
        r_p, r_sr = OrderedDict(), OrderedDict()
        sr_added = False
        for s in sensors:
            csv_data = pd.read_csv(os.path.join(p_emp_dir, s + ".csv"))

            # Add empatica baseline
            d = np.asarray(
                csv_data[csv_data["event"] == "baseline"][["timestamp"] + [
                str(x) for x in range(sensors_all_n_vals[s])]])
            r_s = {"baseline":preprocess_song_data(d,s,acceleration_magnitude)}

            # For each song
            for i in range(n_songs_per_participant):
                id_n = i + 1
                id_str = "song" + str(id_n)

                # Add empatica data
                d = np.asarray(csv_data[csv_data["event"] == id_str][[
                  "timestamp"]+[str(x) for x in range(sensors_all_n_vals[s])]])

                # If there is data, add it
                if np.prod(d.shape) != 0:
                    r_s[id_n] =preprocess_song_data(d,s,acceleration_magnitude)
                else:
                    # print("No X data for song", p, i + 1, "- skipping...")
                    continue

                # Add self-report data
                if not sr_added:
                    sr_csv=pd.read_csv(os.path.join(p_sr_dir,str(id_n)+".csv"))
                    sr_data = sr_csv[-len(emo_labels_all):].values[:, 1]
                    for j in range(len(sr_data)):
                        if sr_data[j] == 'Miss':
                            sr_data[j] = emo_miss_values[j]
                    r_sr[id_n] = sr_data.astype(float)

            sr_added = True
            r_p[s] = r_s
        r_p["selfreport"] = r_sr
        res[p] = r_p

    return res


def shuffle_data(data_dict):
    keys = list(data_dict.keys())
    new_dict = deepcopy(data_dict)
    sensors = list(data_dict[keys[0]].keys())
    for k in keys:
        song_range = range(len(data_dict[k][sensors[0]]) - 1)
        song_is = list(song_range)
        np.random.shuffle(song_is)
        for i in song_range:
            for s in sensors + ["selfreport"]:
                new_dict[k][s][i + 1] = data_dict[k][s][song_is[i] + 1]

    data_inds = list(range(len(keys)))
    np.random.shuffle(data_inds)
    itemz = list(new_dict.items())
    return OrderedDict([itemz[data_inds[i]] for i in range(len(keys))])
