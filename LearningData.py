
#
#  Learning data utilitions
#

from Data import *
from LearningConstants import *

from scipy.interpolate import interp1d


# Windowing function for a single windowing configuration
def _w_feats(data, window_config, globalz):

    # Unpack data and get each window's start time
    feature_funcs, fade_out_buffer, interpol_start, final_size, final_starts, final_ends, final_tss = globalz
    tss, max_ts, d = data
    _subj, _song, sensor, (size, overlap, shift) = window_config
    incr = size * overlap
    w_starts = np.arange(0, max_ts - size + 1e-10, incr)

    # Time shift
    d_ = np.copy(d)
    if shift != 0:
        start_i = 0
        first_ts = 0
        while tss[start_i] < first_ts + shift:
            start_i += 1
        if start_i > 20: # If there's enough data resolution, just shift by measurement index
            d_[start_i:] = d_[:-start_i]
            d_[:start_i] = d_[start_i]
        else:            # Otherwise, interpolate
            new_tss = tss[:-start_i] + shift
            fs = [interp1d(tss, d_[:, i].flatten(), kind='cubic') for i in range(d.shape[1])]
            new_d = np.asarray([fs[i](new_tss) for i in range(len(fs))]).T
            d_ = np.vstack([np.tile(new_d[0], (start_i, 1)), new_d])

    # Apply feature functions & generate windowed data
    d_i, w_data = 0, []
    for w_start in w_starts:
        w_end = w_start + size
        w_ts = w_start + (size / 2)
        d_i_ = d_i
        while d_i < len(tss) and tss[d_i] < w_end:
            d_i += 1
        d_win = d_[d_i_:d_i]
        d_win_ = d_win
        if d_win_.shape[0] == 1:
            d_win_ = d_[d_i_:d_i + 1]
        if d_win_.shape[0] == 1:
            d_win_ = d_[[d_i_, d_i_]]
        w_data.append(np.hstack([w_ts] + [
            np.apply_along_axis(func, 0, d_win_ if func in deriv_funcs else d_win) for func in feature_funcs]))
    w_data = np.vstack(w_data)
    w_tss = w_data[:, 0].flatten()
    if len(w_tss) < 2:
        print(sensor, size, overlap, shift)
    w_d = w_data[:, 1:]

    # Resize into final window size & overlap
    # If there are lots of data points per final window (> interpol_start), take mean
    if final_size / size > interpol_start:

        d_i, fw_data = 0, []
        for fw_i in range(len(final_starts)):
            fw_start = final_starts[fw_i]
            fw_end = final_ends[fw_i]
            fw_ts = final_tss[fw_i]
            d_i_ = d_i
            while d_i < len(w_tss) and w_tss[d_i] < fw_end:
                d_i += 1
            if d_i == d_i_:
                print("Size error! Appending previous values...")
                fw_data.append(fw_data[-1])
                continue
            fw_data.append(np.mean(w_d[d_i_:d_i], axis=0))
        return np.vstack(fw_data)

    # else use cubic interpolation
    fw_f = [interp1d(w_tss, w_d[:, i].flatten(), kind=('cubic' if len(w_tss) >= 4 else 'linear'), bounds_error=False,
                     fill_value=(float(w_d[0, i]), float(w_d[-1, i]))) for i in range(w_d.shape[1])]
    return np.vstack([fw_f[i](final_tss) for i in range(len(fw_f))]).T

windowing_cache = {}

# Compute windowed features
def windowed_features(data_list, base_labels, final_size, final_overlap, window_configs, incl_t,
        feature_funcs=feature_funcs_all, sensors=sensors_all, fade_out_buffer=10, interpol_start=3,
        cache_key="default"):
    global windowing_cache
    if cache_key not in windowing_cache:
        windowing_cache[cache_key] = {}

    # data_list.shape = [n_subjects, n_sensors, n_timestamps, n_readings]

    # window_configs.shape = [n_sensors, n_window_configs_for_sensor, 
                              # len([size, overlap, shift])]

    # res = [n_subjects, n_songs, data_song_length / final_size, (n_sensors *
    #   n_window_configs_for_sensor * n_feature_functions * n_baseline_cases) + int(incl_t)],

    X_labels = ["timeSinceSongStart"]
    for s in sensors:
        for w in range(len(window_configs[s])):
            for f in feature_funcs:
                for b in base_labels[1:]:
                    X_labels.append(b + '_' + s + "_win" + str(w + 1) + '_' + f.__name__)

    n_subjects = len(data_list)
    n_sensors = len(sensors)
    n_songs = [len(data_list[i][sensors[0]]) - 1 for i in range(n_subjects)]
    n_feature_funcs = len(feature_funcs)

    final_incr = final_size * final_overlap
    final_starts = np.arange(0, data_song_length - fade_out_buffer - final_size + 1e-10, final_incr)
    final_ends = final_starts + final_size
    final_tss = final_starts + (final_size / 2)
    globalz = feature_funcs, fade_out_buffer, interpol_start, final_size, final_starts, final_ends, final_tss

    p_configs = sum([sum([sum([[(i, j, k, tuple(window_configs[k][wc_i])) for wc_i in \
                    range(len(window_configs[k]))] for k in sensors], []) \
                     for j in range(n_songs[i])], []) for i in range(n_subjects)], [])

    cached_is, noncached_is = [], []
    for p_i in range(len(p_configs)):
        if p_configs[p_i] in windowing_cache[cache_key]:
            cached_is.append(p_i)
        else:
            noncached_is.append(p_i)

    p_data = {}
    for i in range(n_subjects):
        p_data[i] = {}
        for j in range(n_songs[i]):
            p_data[i][j] = {}
            for k in sensors:
                data = data_list[i][k][j + 1]
                tss = (data[:, 0] - min(data[:, 0])).flatten()
                max_ts = max(tss)
                d = data[:, 1:]
                p_data[i][j][k] = [tss, max_ts, d]

    new_wins = Parallel(n_jobs=n_cpu)(delayed(_w_feats)(
        p_data[p_configs[p_i][0]][p_configs[p_i][1]][p_configs[p_i][2]],
        p_configs[p_i], globalz) for p_i in noncached_is)
    
    win_data = []
    for p_i in range(len(p_configs)):
        if p_i in cached_is:
            win_data.append(windowing_cache[cache_key][p_configs[p_i]])
        else:
            new_win = new_wins[noncached_is.index(p_i)]
            windowing_cache[cache_key][p_configs[p_i]] = new_win
            win_data.append(new_win)

    final_data, w_i = [], 0
    for i in range(n_subjects):
        subj_fwd = []  # Subject final window data
        for j in range(n_songs[i]):
            song_fwd = []
            for k in sensors:
                for w in range(len(window_configs[k])):
                    song_fwd.append(win_data[w_i])
                    w_i += 1
            subj_fwd.append(np.hstack(([final_tss.reshape(-1, 1)] if incl_t else []) + song_fwd))
        final_data.append(subj_fwd)
    return final_data, X_labels

w_config_scaling = w_config_ranges[:, :, 1] - w_config_ranges[:, :, 0]

def random_window_conf(s_i):
    return scale_window_conf(np.random.uniform(0, 1, 3), s_i)

def scale_window_conf(x, s_i):
    return np.maximum(0, np.round((x * w_config_scaling[s_i]) + w_config_ranges[s_i, :, 0], 4))

# Concatenate all windows into one huge feature vector per song
def concat_window_features(data_win):
    data_cw = deepcopy(data_win)
    for i in range(len(data_cw)):
        for j in range(len(data_cw[i])):
            data_cw[i][j][0] = data_cw[i][j][0].reshape(1, -1)
    return data_cw


# Create binary classes for each metric
def even_class_div(data, normalize=False):
    # For each emotion label, find the value which most evenly divides the data
    d = [np.asarray([
        data[j]["selfreport"][k + 1] for k in range(len(data[j]["selfreport"]))]) for j in range(len(data))]
    d_new = deepcopy(d)
    if normalize: # if normalize
        for i in range(len(d_new)):
            d_new[i] = d_new[i] - np.mean(d_new[i], axis=0)
            d_new[i] = d_new[i] / np.std(d_new[i], axis=0)
    d_binary = deepcopy(d_new)
    max_rand_accs, split_vals = [], []
    full_ys = np.vstack(d_new)
    for i in range(len(emo_labels_all)):
        all_ys = full_ys[:, i]
        mean_val = np.mean(all_ys)
        split_val = np.percentile(all_ys, 50)
        split_val += np.random.uniform(0, 0.5) - (
            0.5 if split_val > mean_val else 0)
        perc_greater = sum(all_ys > split_val) / len(all_ys)
        max_rand_accs.append(0.5 + abs(0.5 - perc_greater))
        # print(emo_labels_all[i], np.round(split_val, 2), np.round(100 * perc_greater, 2))
        for j in range(len(d_binary)):
            d_binary[j][:, i] = (d_binary[j][:, i] > split_val).astype(int)
        split_vals.append(split_val)
    return d_new, d_binary, np.asarray(split_vals), max_rand_accs

# Compute and append normalized data for given baselines
def baseline_normalize(data_list,
        orig=True,
        global_baseline=True, global_song_baseline=True,
        subject_baseline=True,  subject_song_baseline=False,
        sensors=sensors_all):
    d = deepcopy(data_list) # New data list with baseline(s)
    if not (subject_baseline or global_baseline):
        return d
    song_baseline = global_song_baseline or subject_song_baseline

    # First get subject baseline averages
    subjs_avgs = defaultdict(list)
    subject_song_avgs = defaultdict(list)
    for i in range(len(d)):
        for s in sensors:
            subject_avgs = np.mean(d[i][s]["baseline"][:, 1:])

            if song_baseline:
                song_avg = np.mean([np.mean(d[i][s][j + 1][:, 1:]) for j in range(len(d[i][s]) - 1)])
            if global_song_baseline or subject_song_baseline:
                subject_song_avgs[s].append(song_avg)
            if global_baseline:
                subjs_avgs[s].append(subject_avgs)

            # If we only need subject baselines, add just these features now
            elif subject_baseline:
                print("ERROR: NOT IMPLEMENTED")
                # for j in range(len(d[i][s]) - 1):
                #     d[i][s][j + 1] = np.hstack(
                #         [d[i][s][j + 1] if orig else d[i][s][j + 1][:, :1]] + \
                #         [d[i][s][j + 1][:, 1:] - subject_avgs])

            if subject_song_baseline and not global_baseline:
                print("ERROR NOT IMPLEMENTED")
                # for j in range(len(d[i][s]) - 1):
                #     d[i][s][j + 1] = np.hstack(
                #         [d[i][s][j + 1]])

    # Add global baseline averages
    if global_baseline:
        global_avgs = np.mean([subjs_avgs[s] for s in sensors], axis=0). \
                        reshape(-1)
        global_song_avgs = np.mean([subject_song_avgs[s] for s in sensors], axis=0).reshape(-1)
        for i in range(len(d)):
            for s_i in range(len(sensors)):
                s = sensors[s_i]
                for j in range(len(d[i][s]) - 1):
                    d[i][s][j + 1] = np.hstack(
                        [d[i][s][j + 1] if orig else d[i][s][j + 1][:, :1]] + \
                        [d[i][s][j + 1][:, 1:] - global_avgs[s_i]] +
                       ([d[i][s][j + 1][:, 1:] - global_song_avgs[s_i]]
                                                if global_song_baseline else []) +
                       ([d[i][s][j + 1][:, 1:] - subjs_avgs[s][i]]
                                                if subject_baseline else []) +
                       ([d[i][s][j + 1][:, 1:] - subject_song_avgs[s][i]]
                                                if subject_song_baseline else []))
    X_labels = ["timeSinceSongStart"]
    if orig:
        X_labels += ["pureSignal"]
    if global_baseline:
        X_labels += ["globalBaseline"]
    if global_song_baseline:
        X_labels += ["globalSongBaseline"]
    if subject_baseline:
        X_labels += ["subjectBaseline"]
    if subject_song_baseline:
        X_labels += ["subjectSongBaseline"]
    return d, X_labels


