#
#  Model Definitions
#

from copy import deepcopy

from sklearn.tree import *
from sklearn.multioutput import *
from sklearn.ensemble import *
from sklearn.model_selection import *
from sklearn.metrics import *
from sklearn.pipeline import *
from sklearn.preprocessing import *
from sklearn.neighbors import *
from sklearn.svm import *
from sklearn.linear_model import *
from sklearn.gaussian_process import *
from sklearn.neural_network import *
from sklearn.decomposition import *
from sklearn.discriminant_analysis import *


# Models library
models_lib = {
    "AdaBoostClassifier": MultiOutputClassifier(AdaBoostClassifier()),
    "AdaBoostRegressor": MultiOutputClassifier(AdaBoostRegressor()),
    "GradientBoostingClassifier":
        MultiOutputClassifier(GradientBoostingClassifier()),
    "GradientBoostingRegressor":
        MultiOutputClassifier(GradientBoostingRegressor()),
    "RandomForestClassifier": MultiOutputClassifier(RandomForestClassifier()),
    "RandomForestRegressor": MultiOutputRegressor(RandomForestRegressor()),
    "DecisionTreeClassifier": MultiOutputClassifier(DecisionTreeClassifier()),
    "DecisionTreeRegressor": MultiOutputRegressor(DecisionTreeRegressor()),

    "KNeighborsRegressor": MultiOutputRegressor(KNeighborsRegressor()),
    "KNeighborsClassifier": MultiOutputClassifier(KNeighborsClassifier()),

    "MLPRegressor": MLPRegressor(),
    "MLPClassifier": MLPClassifier(),

    "SVC": MultiOutputClassifier(SVC()),
    "SVR": MultiOutputRegressor(SVR()),
    # "LinearSVC": MultiOutputClassifier(LinearSVC()),
    "LinearSVC": MultiOutputClassifier(LinearSVC()),
    "LinearSVR": MultiOutputRegressor(LinearSVR()),

    "MultiTaskLasso": MultiTaskLasso(),
    "MultiTaskLassoCV": MultiTaskLassoCV(),
    "MultiTaskElasticNet": MultiTaskElasticNet(),
    "MultiTaskElasticNetCV": MultiTaskElasticNetCV(),
    "LogisticRegression": MultiOutputClassifier(LogisticRegression()),
    "LogisticRegressionCV": MultiOutputClassifier(LogisticRegressionCV()),

    "GaussianProcessClassifier": MultiOutputClassifier(
        GaussianProcessClassifier(n_restarts_optimizer=5)),
    "GaussianProcessRegressor": MultiOutputClassifier(
        GaussianProcessRegressor(n_restarts_optimizer=5)),

    "LinearDiscriminantAnalysis": MultiOutputClassifier(
        LinearDiscriminantAnalysis()),
    "QuadraticDiscriminantAnalysis": MultiOutputClassifier(
        QuadraticDiscriminantAnalysis()),
}

# Add versions of each model with prior dimensionality reductions
_ndim = 100 # Latent space dimensionality
for key in list(models_lib.keys()):
    models_lib[key + "+PCA"] = Pipeline(steps=[
        ("pca", IncrementalPCA(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCAlinear"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='linear', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCArbf"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='rbf', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCApoly"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='poly', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+KPCAsigmoid"] = Pipeline(steps=[
        ("pca", KernelPCA(kernel='sigmoid', n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+FA"] = Pipeline(steps=[
        ("pca", FactorAnalysis(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+NMF"] = Pipeline(steps=[
        ("pca", NMF(n_components=_ndim)),
        ("model", models_lib[key]),
    ])
    models_lib[key + "+LinearDA"] = Pipeline(steps=[
        ("pca", LinearDiscriminantAnalysis(n_components=_ndim)),
        ("model", models_lib[key]),
    ])


# Canoncial models to test
models_c = [
    "LogisticRegression",
    "MLPClassifier",
    "AdaBoostClassifier",
    # "GradientBoostingClassifier",
    # "RandomForestClassifier",
    # "DecisionTreeClassifier",
    # "SGDClassifier",
    # "SVC",
    # "GaussianProcessClassifier",
    # "KNeighborsClassifier",
]
models_r = [
    "MLPRegressor",
    "AdaBoostRegressor",
    # "RandomForestRegressor",
    # "DecisionTreeRegressor",
    # "KNeighborsRegressor",
    # "SGDRegressor",
    # "GaussianProcessRegressor",
]


def get_model(m):
    return deepcopy(models_lib[m])

def scalify_model(m, scaler=RobustScaler):
    return Pipeline(steps=[
        ("scale", scaler()),
        ("model", m),
    ])

