# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/	

import Tkinter as tk
from time import gmtime, strftime
import os
import csv
import spotipy
import spotipy.util as util
import time
import sys
import pandas as pd


LARGE_FONT= ("Verdana", 12)

# file_name = 'TobyLi'
# file_name = 'XinyueZhang'
# file_name = 'ZhilinPan'
# file_name = 'YuanZhang'
# file_name = 'ZeyuSong'
# file_name = 'NianChuanChen'
# file_name = 'JiaxuanLiu'

file_name = 'demo'


save_path = './timestamp'

bg_color = "#DCD0FF"

class SeaofBTCapp(tk.Tk):

    def __init__(self,file_name,save_path,spotify,*args, **kwargs):
        
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.filename   = file_name
        self.save_path = save_path
        self.spotify = spotify

        self.frames = {}
        self.baseline_time = 10 #TIMECHANGE

        for F in (StartPage, PageOne, PageTwo,Count_down):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        if str(cont) == '__main__.Count_down':
            self.get_time('SS')
            frame.countdown(self.baseline_time)        
            frame.tkraise()
        elif str(cont) == '__main__.PageTwo':
            frame.tkraise()
            frame.play_music_master()
        else:
            frame.tkraise()

    def get_time(self,tag_name):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = [tag_name,str(time)]
        self.write_to_csv(data)
    
    def pause_button(self,pause_text):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = [pause_text.get(),str(time)]
        self.write_to_csv(data)
        if pause_text.get() == "PAUSE":
            pause_text.set("RESUME")
        elif pause_text.get() == "RESUME":
            pause_text.set("PAUSE")

    def read_csv(self):
        data = []
        file_name = self.filename + '.csv' 
        file_path = (os.path.join(self.save_path,file_name))

        with open(file_path) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                data.append(row)
        return data

    def write_to_csv(self,data):
        file_name = self.filename + '.csv' 

        file_path = (os.path.join(self.save_path,file_name))
        tmp_data = data
        if os.path.isfile(file_path): 
            tmp_data = self.read_csv()
            tmp_data.append(data) 
            with open(file_path, "wb") as f:
                writer = csv.writer(f)
                for i in tmp_data:
                    writer.writerow(i)
        else:
            with open(file_path, "wb") as f:
                writer = csv.writer(f)
                writer.writerow(tmp_data)

        f.close()
        return 

        
class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="Start Page", font=LARGE_FONT)
        label.config(background=bg_color)
        label.pack(pady=10,padx=10)

        se_button = tk.Button(self, text= "1.Start Empatica", 
                            command=lambda: controller.get_time('SE'),
                            highlightbackground=bg_color)
        se_button.config(background=bg_color)
        se_button.pack()
        
        button = tk.Button(self, text="2.Ready For Experiment",
                            command=lambda: controller.show_frame(PageOne),
                            highlightbackground=bg_color)
        button.config(background=bg_color)
        button.pack()

        re_button = tk.Button(self, text= "Report Empatica Faliure", 
                            command=lambda: controller.get_time('REF'),
                            highlightbackground=bg_color)
        re_button.config(background=bg_color)
        re_button.pack()

        pause_text = tk.StringVar()
        pause_button = tk.Button(self, textvariable= pause_text, 
                            command=lambda: controller.pause_button(pause_text),
                            highlightbackground=bg_color)
        pause_button.config(background=bg_color)
        pause_text.set("PAUSE")  
        pause_button.pack()
        self.configure(background=bg_color)


class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Welcome, Please sit back and relax for two mintues", font=LARGE_FONT)
        label.config(background=bg_color)
        label.pack(pady=10,padx=10)

        button2 = tk.Button(self, text="OK",
                            command=lambda: controller.show_frame(Count_down),
                            highlightbackground=bg_color)
        button2.config(background=bg_color)
        button2.pack()

        button1 = tk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage),
                            highlightbackground=bg_color)
        button1.config(background=bg_color)
        button1.pack()
        self.configure(background=bg_color)


class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        #---------------Song Control--------------------
        self.songs = []

        with open('%s.csv'%file_name) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                self.songs.append(row)
        self.number_of_remaining_songs = len(self.songs) -1
        self.controller = controller


        self.current_song = 1
        self.song_sample_secs = 40 #60*2  #TIMECHANGE
        self.silence_secs = 10 #30  #TIMECHANGE

        self.volume_reduce_point = 20

        self.state = "Silence"

        self.song_state = True
        self.song_remaining = 0
                
        self.silence_state = True
        self.silence_remaining = 0

        
        #---------------------------Layout ---------------------------
        self.label_song_id = tk.Label(self, text=" ", font=LARGE_FONT)
        self.label_song_id.config(background=bg_color)
        self.label_song_id.pack(pady=10,padx=10)

        self.label_time_count_down = tk.Label(self, text=" ", font=LARGE_FONT)
        self.label_time_count_down.config(background=bg_color)
        self.label_time_count_down.pack(pady=10,padx=10)

        self.pause_text = tk.StringVar()
        re_button = tk.Button(self, text= "Report Empatica Faliure", 
                            command=lambda: self.report_ref_music(self.pause_text),
                            highlightbackground=bg_color)
        re_button.config(background=bg_color)
        re_button.pack()

        self.pause_button = tk.Button(self, textvariable= self.pause_text, 
                            command=lambda: self.pause_button_count_down(self.pause_text),
                            highlightbackground=bg_color)
        self.pause_button.config(background=bg_color)
        self.pause_text.set("PAUSE")  
        self.pause_button.pack()

        self.button1 = tk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage),
                            highlightbackground=bg_color)
        self.button1.config(background=bg_color)
        self.button1.pack()
        self.button1.config(state="disabled")
        self.configure(background=bg_color)

        
    def report_ref_music(self,pause_text):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = ["REF",str(time)]
        self.controller.write_to_csv(data)
        if self.state == "Silence":
            self.silence_state = False
            self.pause_text.set("RESUME")


        elif self.state == "Playing":
            self.song_state = False
            self.controller.spotify.pause_music()
            self.current_song = self.current_song - 1
            self.number_of_remaining_songs = self.number_of_remaining_songs + 1
            self.pause_text.set("Replay Last Song")



    def pause_button_count_down(self,pause_text):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = [pause_text.get(),str(time)]
        self.controller.write_to_csv(data)

        if pause_text.get() == "PAUSE":
            if self.state == "Silence":
                self.silence_state = False
                self.pause_text.set("RESUME")

            elif self.state == "Playing":
                self.song_state = False
                self.controller.spotify.pause_music()
                self.current_song = self.current_song - 1
                self.number_of_remaining_songs = self.number_of_remaining_songs + 1
                self.pause_text.set("Replay Last Song")

        elif pause_text.get() == "RESUME":
            self.silence_state = True
            self.pause_text.set("PAUSE")


        elif pause_text.get() == "Replay Last Song":
            pause_text.set("PAUSE")
            if self.state == "Silence":
                self.slience_state = True
            elif self.state == "Playing":
                self.song_state = True
                self.song_remaining = self.song_sample_secs
                self.play_music()


    def countdown(self, silence_remaining = None):
        if self.silence_state == True:
            if silence_remaining is not None:
                self.silence_remaining = silence_remaining

            if self.silence_remaining <= 0:
                if self.number_of_remaining_songs > 0:
                    self.play_music_master()
                else:
                    self.label_time_count_down.configure(text="That's the end of the experiment! Thank you very much!")
                    self.button1.config(state="normal")
            else:
                self.label_time_count_down.configure(text="Now you have %d seconds to complete self report. Thank you!" % self.silence_remaining)
                self.silence_remaining = self.silence_remaining - 1
                self.after(1000, self.countdown)
        else:
            self.master.after(100, self.countdown)
    
    def song_dur(self,song_remaining = None):
        if self.song_state == True:
            if song_remaining is not None:
                self.song_remaining = song_remaining
                full_remaining = song_remaining

            if self.song_remaining <= 0:
                self.controller.spotify.pause_music()
                stop_song_id = self.current_song - 1
                self.controller.get_time("Stop_Song_%i" % stop_song_id)
                self.state = "Silence"
                self.label_time_count_down.configure(text="Now you have 1 mintues to complete self report")
                self.countdown(self.silence_secs)
            else:
                if self.song_remaining < self.volume_reduce_point:
                    new_volume = 5 * self.song_remaining
                    self.controller.spotify.control_volume(new_volume)
                self.song_remaining = self.song_remaining - 1
                self.after(1000, self.song_dur)
        else:
            self.master.after(100, self.song_dur)

    def play_music_master(self):
        self.play_music()
        self.song_dur(self.song_sample_secs)

    def play_music(self):
            self.state = "Playing"
            self.number_of_remaining_songs = self.number_of_remaining_songs - 1
            self.controller.get_time("Start_Song_%i" % self.current_song)
            self.label_song_id.configure(text="Song_id: %i" % self.current_song)    
            self.label_time_count_down.configure(text="Now enjoy the music")

            # start track
            self.controller.spotify.play_music_spotify(self.songs[self.current_song][1],self.songs[self.current_song][2])
            self.current_song = self.current_song + 1
       

class Count_down(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.label = tk.Label(self, text="", width=20)
        self.label.config(background=bg_color)
        self.label.pack(pady=10,padx=10)
        self.controller = controller

        self.button1 = tk.Button(self, text="Start the first song",
                            command=lambda: controller.show_frame(PageTwo),
                            highlightbackground=bg_color)
        self.button1.config(background=bg_color)
        self.button1.pack()
        self.button1.config(state="disabled")

        self.pause_text = tk.StringVar()
        re_button = tk.Button(self, text= "Report Empatica Faliure", 
                            command=lambda: self.report_ref(self.pause_text),
                            highlightbackground=bg_color)
        re_button.config(background=bg_color)
        re_button.pack()

        self.pause_button = tk.Button(self, textvariable= self.pause_text, 
                            command=lambda: self.pause_button_count_down(self.pause_text),
                            highlightbackground=bg_color)
        self.pause_button.config(background=bg_color)
        self.pause_text.set("PAUSE")  
        self.pause_button.pack()

        self.state = True
        self.remaining = 0
        self.baseline_time = controller.baseline_time
        self.configure(background=bg_color)

    def report_ref(self,pause_text):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = ["REF",str(time)]
        self.controller.write_to_csv(data)
        pause_text.set("RESTART")
        self.state = False

    def pause_button_count_down(self,pause_text):
        time = strftime("%Y-%m-%d-%H:%M:%S", gmtime())
        data = [pause_text.get(),str(time)]
        self.controller.write_to_csv(data)

        if pause_text.get() == "PAUSE":
            pause_text.set("RESTART")
            self.state = False

        elif pause_text.get() == "RESTART":
            pause_text.set("PAUSE")
            b1_state = str(self.button1['state'])
            if b1_state == "normal":
                self.state = True
                self.countdown(self.baseline_time)
                self.button1.config(state="disabled")     
            else:
                self.state = True
                self.remaining = self.baseline_time

    def countdown(self, remaining = None):
        if self.state == True:
            if remaining is not None:
                self.remaining = remaining

            if self.remaining <= 0:
                self.label.configure(text="time's up! TIme for music")
                self.button1.config(state="normal")
            else:
                self.label.configure(text="Time remianing: %d seconds" % self.remaining)
                self.remaining = self.remaining - 1
                self.after(1000, self.countdown)
        else:
            self.master.after(100, self.countdown)

class Music_player():
    def __init__(self,token):
        self.spotify = spotipy.Spotify(token)
        try:        
            self.spotify.pause_playback()
        except:
            pass

    def play_music_spotify(self,spotify_uri,start_point):
        start_point = int(start_point)
        self.spotify.start_playback(uris=[spotify_uri])
        self.spotify.seek_track(position_ms=start_point*1000)
        self.spotify.volume(100)

    def pause_music(self):
        self.spotify.pause_playback()   

    def control_volume(self,new_volume):
        self.spotify.volume(new_volume) 



SPOTIPY_CLIENT_ID='d3126bb4e7514ef6b989c1eb71aee15d'
SPOTIPY_CLIENT_SECRET='e4f33babc26945d4b6c5372056cc3e0d'
SPOTIPY_REDIRECT_URI='https://example.com/callback/'

scope = 'user-library-read user-modify-playback-state'
# username = 'xinyuezhang'
username = sys.argv[1]


cache_file_name = '.cache-' + username

if os.path.exists(cache_file_name):
    os.remove(cache_file_name)

token = util.prompt_for_user_token(username,
                                   scope,
                                   client_id=SPOTIPY_CLIENT_ID,
                                   client_secret=SPOTIPY_CLIENT_SECRET,
                                   redirect_uri='http://localhost/')


spoyify_thread = Music_player(token)
app = SeaofBTCapp(file_name,save_path,spoyify_thread)
app.mainloop()