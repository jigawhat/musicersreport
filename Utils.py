'''
    
    Utility methods

'''

import joblib
from joblib import Parallel, delayed
from copy import deepcopy
from collections import defaultdict
from threading import Thread, Condition

from UniversalUtils import *
from Constants import *


def save_ld(data, name):
    create_folder(data_dir)
    create_folder(os.path.join(data_dir, learning_data_dir))
    joblib.dump(data, os.path.join(data_dir, learning_data_dir, name + ".data"))

def load_ld(name):
    return joblib.load(os.path.join(data_dir, learning_data_dir, name + ".data"))


def save_json(obj, name, pad=True):
    if pad:
        name = os.path.join(data_dir, name + '.json')
    with open(name, 'w') as outfile:
        json.dump(obj, outfile)

def load_json(name, pad=True):
    if pad:
        name = os.path.join(data_dir, name + '.json')
    try:
        with open(name, 'r') as infile:
            return json.load(infile)
    except:
        # print("Error loading json file [" + name + "]:")
        # print(sys.exc_info())
        # print(sys.exc_info()[1])
        # print(sys.exc_info()[2])
        return None



#
#  db_add
#
#  Adds (concatenates) a dataframe to a csv database & returns updated database
#
#  df               = new data to add
#  path             = path of database
#  return_region    = if not None, region for which to return data for
#  **kwargs         = keyword args for database-loading pd.read_csv call
#
def db_add(df, path, **kwargs):
    new_df = None
    if os.path.exists(path):
        with open(path, 'a') as db:
            df.to_csv(db, header=False)
        new_df = pd.read_csv(path, **kwargs)
    else:
        df.to_csv(path)
        new_df = df
    return new_df

#
#  db_upd
#
#  Updates (merges) a dataframe into a csv database & returns updated database
#  Only difference to db_add is that df can contain new data for existing keys
#
def db_upd(df, path, **kwargs):
    new_df = None
    if os.path.exists(path):
        new_df = pd.read_csv(path, **kwargs)
        for key in list(df.index):
            new_df.loc[key] = list(df.loc[key])
        new_df.to_csv(path)
    else:
        df.to_csv(path)
        new_df = df
    return new_df


