#
#  Graphics library
#

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.ticker import FuncFormatter

from Utils import *


# Print mean scores for a subset of Y_labels over all increments
def print_learning_curve_mean_scores(models, scorers, scores, Y_l, to_pr=None):
    for i in range(len(models)):
        print("\t" + models[i])
        for l in range(len(scorers)):
            res = deepcopy(scores[i][l])
            for j in range(len(res)):
                res[j] = np.mean(res[j], axis=0)
            if len(to_pr) > 1:
                res = np.array(res)[:,[Y_l.index(o) for o in to_pr]]
                res = np.mean(res, axis=1)
            elif len(to_pr) == 1:
                res_arr = np.array(res)
                if res_arr.ndim == 1:
                    res_arr = res_arr.reshape((-1, 1))
                res = res_arr[:,Y_l.index(to_pr[0])]
            else:
                res = np.mean(res, axis=1)
            pr(scorers[l] + ": " + str(fm_nums(res, 3)))


# Helper functions for graph_learning_curve
def prep_lc_plot_(plt, n_rows, n_cols, m, ns_samples, y_label_prefix, scorer):
    ax = plt.subplot(n_rows, n_cols, m + 1)
    plt.ylabel(y_label_prefix + ' ' + scorer_label_format[scorer])
    plt.xlabel("N training samples")
    ax.set_xticks(ns_samples if len(ns_samples) < 15 else \
        (ns_samples[:-2:3] + [ns_samples[-1]]))
    ax.grid(alpha=0.5)
    return ax

def plot_lc_(plt, ax, data, errs, ns_samples,
             error_bars, handles, legend_label, l, i):
    p = plt.plot(ns_samples, data)
    col = p[0].get_color()
    if error_bars:
        ax.errorbar(ns_samples, data, yerr=2 * np.array(errs), ecolor=col,
                    alpha=0.15, capsize=5)
    if l == 0 and i == 0:
        handles.append(mpatches.Patch(color=col, label=legend_label))

# Plot learning curves for all models, scorers & Y targets
def graph_learning_curve(models, scorers, scores, ns_samples, Y_labels,
        size_factor=1.0, error_bars=True, incl_Y=None,
        incl_models=None, filename=None, compare_models=False):
    if incl_models is None:
        incl_models = models
    if incl_Y is None:
        incl_Y = Y_labels
    fig = plt.gcf()
    n_cols = 1 if ((compare_models and len(incl_Y) == 1) or \
        ((not compare_models) and len(incl_models) == 1)) else 2
    n_runs = len(scores[0][0][0])
    plt.suptitle("Learning curve" + ('s' if n_cols > 1 else '') + \
        " (% accuracy, mean over " + str(n_runs) + \
        " runs $\pm 2\sigma$ error bars)", size=15, y=0.9)
    n_rows = int(np.ceil(len(scorers) / 2)) * \
        len(Y_labels if compare_models else incl_models)
    fig.set_size_inches(16 * size_factor,
        (.3 + (4.5 * n_rows)) * size_factor * (2.5 if n_cols == 1 else 1))
    m, handles = 0, []
    if compare_models: # On each graph, compare models instead of Y targets
        results = [None] * len(Y_labels)
        results_stds = [None] * len(Y_labels)
        for i in range(len(models)):
            for l in range(len(scorers)):
                res = deepcopy(scores[i][l])
                res_stds = []
                for j in range(len(res)): # Average over repeat runs
                    res_stds.append(np.std(res[j], axis=0))
                    res[j] = np.mean(res[j], axis=0)
                res = np.array(res).T
                res_stds = np.array(res_stds).T
                for j in range(len(Y_labels)):
                    if results[j] is None:
                        results[j] = [[] for k in range(len(scorers))]
                    if results_stds[j] is None:
                        results_stds[j] = [[] for k in range(len(scorers))]
                    results[j][l].append(res[j])
                    results_stds[j][l].append(res_stds[j])
        for i in range(len(Y_labels)):
            y_label = Y_labels[i]
            if y_label not in incl_Y:
                continue
            for l in range(len(scorers)):
                r = results[i][l]
                r_stds = results_stds[i][l]
                ax = prep_lc_plot_(plt, n_rows, n_cols, m, ns_samples,
                    Y_labels_format[y_label], scorers[l])
                m += 1
                for n in range(len(models)):
                    if models[n] not in incl_models:
                        continue
                    plot_lc_(plt, ax, r[n], r_stds[n], ns_samples, error_bars,
                        handles, model_label_format[models[n]], l, i)
    else:
        for i in range(len(models)):
            model = models[i]
            if model not in incl_models:
                continue
            for l in range(len(scorers)):
                res = deepcopy(scores[i][l])
                res_stds = []
                for j in range(len(res)):
                    res_stds.append(np.std(res[j], axis=0))
                    res[j] = np.mean(res[j], axis=0)
                ax = prep_lc_plot_(plt, n_rows, n_cols, m, ns_samples,
                    model_label_format[model], scorers[l])
                m += 1
                if type(res[0]) is not list and type(res[0]) is not np.ndarray:
                    r = [res]
                    r_stds = [res_stds]
                else:
                    r = list(zip(*res))
                    r_stds = list(zip(*res_stds))
                for n in range(len(Y_labels)):
                    if Y_labels[n] not in incl_Y:
                        continue
                    plot_lc_(plt, ax, r[n], r_stds[n], ns_samples, error_bars,
                        handles, Y_labels_format[Y_labels[n]], l, i)

    plt.legend(handles=handles, bbox_to_anchor=(0.99, 0.3))
    if filename is not None:
        plt.savefig(graphs_folder + filename + '.pdf', format='pdf')
    plt.show()


