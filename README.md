# MusicERS
Music Emotion Recognition System

## COMPGI17 Mini-Project: Team Wubba Lubba Dub Dub
This repository is submitted in fulfillment of the software requirement of the COMPGI17 (Affective Computing & Human-Robot Interaction) mini-project in April 2018.

The team members are:
* Lynray Barends
* Nianchuan Chen
* Taolun Li
* Richard Sterry
* Alfonso White
* Xinyue Zhang

The codebase includes:
* [Experiment GUI](#experiment-gui) This GUI controls the music emotion induction experiments, playing the subjects carefully-designed playlists using the Spotipy API.
* [Machine Learning Code](#machine-learning-code) This code:
  * loads, synchronizes and normalizes the experimental data
  * creates features
  * trains and optimizes various machine learning models
  * evaluates the performance of the resulting models

Note: To request a full version (anonymised) of the dataset used, please message me (alfy#9403) on discord, or email alfewhite at gmail.com.

<hr>

## Experiment GUI

The GUI we used to play music and collect time stamp. 
See in the './GUI' folder

### Prerequisites:
Install the spotify api for python: [spotipy](https://github.com/plamere/spotipy)

### How to run it:
1. Open  spotify app in your computer

2. Find out your spotify username

3. a. If you want to see a demo of the whole experiment protocol, please run: `python demo.py your_spotify_username`

3. b. If you wish to complete an full experiment, please run: `python experiment_gui.py your_spotify_username`

4. After you run the python command, it will driect you to a webpage, copy the link of that page, and paste into terminal, press enter.

5. Now you can see the GUI!

## Self_report_gui

This is a simple gui we created for convert self report value into csv file.
See in the './Self_Report_GUI' folder

### How to run it:

`python self_report_collect.py`

<hr>

## Machine Learning Code

Please open and run the script `run_all.py` to run the full feature extraction, selection and modelling pipeline for start to finish. The script contains some useful options for pipeline evaluation. The full pipeline itself is defined in `FinalModel.py` and is well commented.

