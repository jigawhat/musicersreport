import unirest
import spotipy.util as util
import os
import csv
import pickle


SPOTIPY_CLIENT_ID='d3126bb4e7514ef6b989c1eb71aee15d'
SPOTIPY_CLIENT_SECRET='e4f33babc26945d4b6c5372056cc3e0d'
SPOTIPY_REDIRECT_URI='https://example.com/callback/'

scope = 'user-library-read user-modify-playback-state'
username = 'xinyuezhang'

token = util.prompt_for_user_token(username,
                                   scope,
                                   client_id=SPOTIPY_CLIENT_ID,
                                   client_secret=SPOTIPY_CLIENT_SECRET,
                                   redirect_uri='http://localhost/')



def read_file(file_name):
    data = []

    with open(file_name, 'rb') as csvfile:
        readers= csv.reader(csvfile,  delimiter=',', quotechar='"')
        x = 0
        for row in readers:   
            if x != 0:
                data.append(row)
            else:
                x = x +1
    return data


def write_to_csv(file_name,data,save_path):
    file_name = file_name + '.csv' 

    file_path = (os.path.join(save_path,file_name ))

    with open(file_path, "w") as f:
        writer = csv.writer(f)
        writer.writerows(data)
    f.close()
    return 


def save_as_pickle(data,file_name):

    with open('%s.pickle'%file_name, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_data_from_pickle(file_name):

    with open('%s.pickle'%file_name, 'rb') as handle:
        data = pickle.load(handle)

    return data

def get_song_feature(spotify_id):
  search_url = "https://api.spotify.com/v1/audio-features/%s"%spotify_id

  response = unirest.get(search_url,
    headers={
      "Authorization": "Bearer %s"%token,
      "Accept": "application/json"
    }
  )
  return response.body


def convert_to_list(index_list,song_feature):
  song_feature_list = []
  for x in index_list:
    song_feature_list.append(song_feature[x])
  return song_feature_list

def folder_walkthrough(folder_path):

  spotify_music_feature_list = []
  spotify_music_feature_dict = {}
  index_list = []


  song_id = 0

  for file in os.listdir(folder_path):
    if file.endswith(".csv"): 
      file_name = (os.path.join(folder_path, file))
      data = read_file(file_name)
      for x in range(0,len(data)):
        spotify_id = data[x][1]
        print song_id,spotify_id
        song_feature = get_song_feature(spotify_id[14:])
        spotify_music_feature_dict[song_id] = song_feature

        if song_id == 0:
          for key in song_feature:
            index_list.append(key)
          spotify_music_feature_list.append(index_list)

        song_feature_list = convert_to_list(index_list,song_feature)
        spotify_music_feature_list.append(song_feature_list)

        song_id = song_id + 1


  write_to_csv('song_features',spotify_music_feature_list,'.')
  save_as_pickle(spotify_music_feature_dict,'song_features')
          




folder_walkthrough('./playlist')                       

